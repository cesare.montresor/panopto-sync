# pylint: disable=bad-continuation
# pylint: disable=bad-whitespace
# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-few-public-methods
# pylint: disable=pointless-string-statement

import sys
import time
import datetime as dt
import re
import json
import itertools
import uuid
from typing import List, Optional, Callable
from urllib.parse import urlparse, urlencode, parse_qs  # , quote
from email.utils import parsedate_to_datetime

# documentation:
#     https://github.com/SeleniumHQ/selenium/blob/master/py/selenium/webdriver/remote/webdriver.py
#     https://github.com/SeleniumHQ/selenium/blob/master/py/selenium/webdriver/remote/webelement.py
from selenium import webdriver

# from selenium.webdriver.remote import webelement
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from selenium.webdriver.support.ui import WebDriverWait

# from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from core import (
    digest,
    mergedict,
    win_to_unix_datetime,
    group_by,
    simple_class,
)
from extract_date import assign_better_date_and_name
from title_format import set_course_name_fields
from web import (
    Cookies,
    get_relevant_cookies,
    keep_valid_cookies,
    merge_newer_cookies,
    probe_http_head,
    probe_http_get,
    probe_http_post,
    gzip_decompress_or_raw,
)
from logs import logger, log, errlog
from classes import Course, Lesson, LessonHint, Stream, Timestamp, Segment  # , Comment


RawEntry = simple_class("url title label header", "RawEntry")


class Credentials:
    def __init__(self, usr: str = "", psw: str = ""):
        self.username = usr
        self.password = psw


class UnsubscribedError(Exception):
    pass


class UniVRBot:
    """Use with [with-as] statement."""

    # DRIVER_LOG = 'geckodriver.log'
    GIASSO_PAGE = "https://giasso.univr.it"
    MOODLE_PAGE_PREFS = "https://moodledidattica.univr.it/user/preferences.php"  # just more slim to load
    MOODLE_COURSES_FMT = "https://moodledidattica.univr.it/lib/ajax/service.php"
    MOODLE_COURSE_PAGE_FMT = "https://moodledidattica.univr.it/course/view.php?id={}"
    MOODLE_RESOURCE_VIEW = "https://moodledidattica.univr.it/mod/resource/view.php"  # id=
    MOODLE_URL_VIEW = "https://moodledidattica.univr.it/mod/url/view.php"  # id=
    PANOPTO_LOGIN_PAGE = "https://univr.cloud.panopto.eu/Panopto/Pages/Auth/Login.aspx"
    PANOPTO_VIEWER_PAGE = "https://univr.cloud.panopto.eu/Panopto/Pages/Viewer.aspx"
    PANOPTO_DELIVERYINFO = "https://univr.cloud.panopto.eu/Panopto/Pages/Viewer/DeliveryInfo.aspx"
    PANOPTO_VIEW_RESULTS = "https://univr.cloud.panopto.eu/Panopto/Pages/Viewer/Search/Results.aspx"
    # PANOPTO_SOCIAL_URL   = 'https://univr.cloud.panopto.eu/Panopto/Podcast/Social/{}.mp4'
    PANOPTO_THUMB_URL = "https://{}/sessions/{}/{}_et/thumbs/slide{}.jpg"
    PANOPTO_SLIDE_URL = "https://{}/sessions/{}/{}_et/images/slide{}.jpg"
    PANOPTO_TUTORIAL_ID = "ef1788d3-46b8-42b9-8f3a-ab6d0116ce35"
    # REQUEST_ASSISTANT_XPI = str(Path('request_assistant-0.3-an+fx.xpi').absolute())
    FAKE_USERAGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:75.0) Gecko/20100101 Firefox/75.0"
    POST_REQ_HEADERS = {
        "Accept-Encoding": "gzip, deflate",
        "Connection": "keep-alive",
    }
    # WARNING: Firefox seems to convert %xx url characters automatically,
    # so space and others chars may be present in a HTML source page.
    RGX_ID = re.compile(r"[?&]id=([^?&/]+)")
    RGX_DATE = re.compile(r"/Date\(([0-9]+)\)/")
    RGX_MPEG_ICO = re.compile(r"\b.*?mpeg-[0-9]+?\b")
    RGX_MOODLE_MP4 = re.compile(
        r"\bhttps://moodledidattica\.univr\.it/pluginfile\.php/[0-9]+/mod_resource/content/[0-9]+/([^?&/]+?)\.mp4\b"
    )
    RGX_NAME_NO_VER = re.compile(r"^(.+?) *#?[0-9.]+\xB0?$")

    def __init__(self, cred: Credentials, cookies: Cookies = None):
        self.cred = cred
        self.driver: webdriver.Firefox  # Useful for LSP support
        self.driver = None
        # self.logfile = None
        # self.logiter = None
        self.known_cookies = cookies
        if cookies is not None:
            keep_valid_cookies(cookies)
            for c in cookies:
                assert isinstance(c.get("name"), str), "Cookie: name is not string"
                assert isinstance(c.get("value"), str), "Cookie: value is not string"

    def __enter__(self):
        self.launch()
        assert self.login_giasso(), "GIASSO: Unable to login"
        assert self.login_panopto(True), "Panopto: Unable to login"
        self.make_sure_moodle_is_working()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.quit()

    def launch(self):
        assert self.driver is None, "Launching: webdriver already exists"
        # if Path(UniVRBot.DRIVER_LOG).is_file():
        #    os.remove(UniVRBot.DRIVER_LOG)
        #    log('Removed web driver log.')
        log("Launching web driver...")
        profile = FirefoxProfile()
        # profile.set_preference('browser.dom.window.dump.enabled', True)
        profile.set_preference("general.useragent.override", UniVRBot.FAKE_USERAGENT)
        options = FirefoxOptions()
        options.add_argument("--headless")
        # options.page_load_strategy = 'eager' # just wait for content loaded
        # options.log.level = 'TRACE' # DO NOT ENABLE - ALSO LOGS LOGIN DATA

        driver = webdriver.Firefox(profile, options=options)
        # driver = webdriver.Firefox(options=options)
        log("Web driver has started.", erase_prev=True)
        # driver.implicitly_wait(3) # seconds before searching any element
        # driver.maximize_window()
        if self.known_cookies is not None:
            log("\nLoading known cookies...")
            with logger:
                domains = sorted([d["domain"] for d in self.known_cookies])
                load_tree = {}
                while domains:
                    d1 = domains.pop()
                    i = 0
                    while domains and i < len(domains):
                        d2 = domains[i]
                        if d1 != d2 and d1.endswith(d2):
                            if d1 not in load_tree:
                                load_tree[d1] = set()
                            load_tree[d1].add(d2)
                            del domains[i]
                            i -= 1
                        i += 1
                    if d1 not in load_tree:
                        load_tree[d1] = set()
                # errlog("cookies:", self.known_cookies, verbos=4)
                # errlog("load_tree:", load_tree, verbos=4)
                # sys.exit(0)

                def key_search(co):
                    d = co["domain"]
                    for k, v in load_tree.items():
                        if d == k or d in v:
                            d = k
                    if d.startswith("."):
                        d = d[1:]
                    return d

                sck = sorted(self.known_cookies, key=key_search)
                for domain, cookies in itertools.groupby(sck, key_search):
                    log(domain, pointer=True)
                    url = f"https://{domain}/404"
                    # with logger:
                    # errlog("load:", url, verbos=1)
                    # ALERT should be commented
                    # errlog("cookies:", list(cookies), verbos=4)
                    driver.get(url)
                    for c in cookies:
                        driver.add_cookie(c)
                        # errlog("done", verbos=1)
            log("Cookies loaded.")
        # driver.install_addon(UniVRBot.REQUEST_ASSISTANT_XPI, temporary=True)
        self.driver = driver
        # self.logfile = open('geckodriver.log')
        # self._logiter = getNextLineIterator(self.logfile, lambda l: self._logfilter(l))

    def quit(self):
        assert self.driver is not None, "Closing: webdriver does not exist"
        self.driver.quit()
        # self.logfile.close()
        self.driver = None
        # self._logiter = None
        log()
        log("Web driver closed.")

    def merge_cookies(self):
        merge_newer_cookies(self.known_cookies, self.driver.get_cookies())

    def get(self, *args, **kwargs):
        """Required to save cookies, page after page"""
        # errlog(*args, kwargs)
        self.driver.get(*args, **kwargs)
        self.merge_cookies()

    def _login_page_giasso(self) -> bool:
        d = self.driver
        c = self.cred
        time.sleep(3)
        if d.title != "OpenSSO (Login)":
            return False
        id_token1 = WebDriverWait(d, timeout=5).until(lambda dr: dr.find_element_by_id("IDToken1"))
        id_token1.clear()
        id_token1.send_keys(c.username)
        id_token2 = WebDriverWait(d, timeout=5).until(lambda dr: dr.find_element_by_id("IDToken2"))
        id_token2.clear()
        id_token2.send_keys(c.password)
        submit = WebDriverWait(d, timeout=5).until(lambda dr: dr.find_element_by_name("Login.Submit"))
        submit.click()
        return True

    def login_giasso(self) -> bool:
        d = self.driver
        self.get(UniVRBot.GIASSO_PAGE)
        log("GIASSO: Logging in...")
        ret = False
        if self._login_page_giasso():
            try:
                WebDriverWait(d, timeout=6).until(
                    lambda dr: dr.title
                    in (
                        "Oracle OpenSSO",
                        "OpenSSO (User has no profile in this organization)",  # should work anyway
                    )
                )
                log("GIASSO: Logged successfully.", erase_prev=True)
                ret = True
            except TimeoutException:  # this course page does not even have a sidebar
                log("GIASSO: Unable to log in.", erase_prev=True)
        elif d.title == "Oracle OpenSSO":
            log("GIASSO: Already logged in.")
            ret = True
        self.merge_cookies()
        return ret

    def login_panopto(self, skip_giasso: bool) -> bool:
        if not skip_giasso and not self.login_giasso():
            return False
        d = self.driver
        self.get(UniVRBot.PANOPTO_LOGIN_PAGE)
        login_panel = WebDriverWait(d, timeout=5).until(
            lambda dr: dr.find_element_by_id("PageContentPlaceholder_loginControl_loginUpdatePanel")
        )
        login = login_panel.find_elements_by_id("PageContentPlaceholder_loginControl_externalLoginButton")

        def search_success():
            try:
                WebDriverWait(d, timeout=5).until(lambda dr: dr.find_element_by_id("loginLink"))
                return True
            except TimeoutException:
                return False

        if len(login) == 1:
            log("Panopto: Logging in with external credentials...")
            login[0].click()
            if not (search_success() or self._login_page_giasso() or search_success()):
                log("Panopto: Unable to log in.", erase_prev=True)
                self.merge_cookies()
                return False
            log("Panopto: Logged successfully.", erase_prev=True)
            self.merge_cookies()
            return True
        # just find logout button, raise error if not present
        login_panel.find_element_by_id("PageContentPlaceholder_loginControl_Logout")
        log("Panopto: Already logged in.")
        self.merge_cookies()
        return True

    def make_sure_moodle_is_working(self, clean_cookies=True):
        d = self.driver
        self.get(UniVRBot.MOODLE_PAGE_PREFS)
        log("Checking if Moodle is working...")
        try:
            WebDriverWait(d, timeout=5).until(lambda dr: dr.find_element_by_class_name("breadcrumb"))
        except TimeoutException as err:
            log("Moodle is not working.", erase_prev=True)
            if not clean_cookies:
                raise err
            log("Erasing relative cookies...")
            urlp = urlparse(UniVRBot.MOODLE_PAGE_PREFS)
            cookies = get_relevant_cookies(self.known_cookies, urlp.netloc, urlp.path)
            for c in cookies:
                self.known_cookies.remove(c)
                d.delete_cookie(c["name"])
            assert self.login_giasso(), "GIASSO (Moodle check): Unable to login"
            return self.make_sure_moodle_is_working(False)
        self.merge_cookies()
        log("Moodle is working.", erase_prev=True)
        return None

    def get_all_courses(self) -> List[Course]:
        """Finds all courses by scraping Moodle main page, and by reversed requests."""
        d = self.driver
        self.get(UniVRBot.MOODLE_PAGE_PREFS)
        logininfo = WebDriverWait(d, timeout=4).until(lambda dr: dr.find_element_by_class_name("logininfo"))
        anchors = logininfo.find_elements_by_tag_name("a")
        userid, sesskey = [parse_qs(urlparse(a.get_attribute("href")).query) for a in anchors]
        userid, sesskey = userid["id"][0], sesskey["sesskey"][0]
        errlog("userid:", userid, verbos=4)
        errlog("sesskey:", sesskey, verbos=4)

        params = [
            {
                "index": 0,
                "methodname": "core_course_get_recent_courses",
                "args": {"userid": userid, "limit": 1000},
            }
        ]
        resp = probe_http_post(
            f"{UniVRBot.MOODLE_COURSES_FMT}?sesskey={sesskey}&info=core_course_get_recent_courses",
            json.dumps(params),
            self.known_cookies,
            mergedict(
                UniVRBot.POST_REQ_HEADERS,
                {
                    "User-Agent": UniVRBot.FAKE_USERAGENT,
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                },
            ),
        )
        assert resp.status == 200, "Response is not successful"
        data = gzip_decompress_or_raw(resp.read()).decode()
        resp.close()
        try:
            info = json.loads(data)[0]
            iserror = info.get("error")
            # errlog("info:", data, verbos=1)
            if iserror:
                exc = info["exception"]
                # errlog("Moodle reports an error:")
                # with logger:
                # errlog(exc["message"], pointer=True)
                errcode = exc["errorcode"]
                if errcode == "invalidsesskey":
                    raise PermissionError(iserror)
                raise RuntimeError(iserror)
            courses = info.get("data")
            if not courses:
                errlog(info)
                raise RuntimeError("Data does not contain courses!")
        except json.JSONDecodeError as err:
            errlog()
            errlog("Not a JSON!")
            raise err

        # ALERT entries contain "summary" and "courseimage", two very big strings

        ret = []
        courses = sorted(courses, key=lambda co: (co["hidden"], co["isfavourite"], co["timeaccess"]))
        for c in courses:
            crs = Course(
                name=c["fullname"],
                id=str(c["id"]),
                lessons={},
            )
            set_course_name_fields(crs)
            ret.append(crs)
        return ret
        # errlog("found:", len(courses), verbos=1)
        # for c in courses:
        #     anchor = c.find_elements_by_css_selector("a.coursename")[0]
        #     link = anchor.get_attribute("href")
        #     name = anchor.find_elements_by_css_selector('span.multiline')[0].get_attribute("innerHTML")
        #     m = UniVRBot.RGX_YEAR.match(name)
        #     year = int(m[3]) if m else None
        #     crs = Course(
        #         name=name,
        #         id=UniVRBot.RGX_ID.findall(link)[0],
        #         year=year,
        #         lessons={},
        #     )
        #     ret.append(crs)
        # ret = set(ret)
        # errlog("found:", ret, verbos=1)
        # return ret

    def probe_moodle_url_view(self, url) -> Optional[str]:
        resp = probe_http_head(url, self.known_cookies, {"User-Agent": UniVRBot.FAKE_USERAGENT})
        link = None
        errlog("moodle url:", url, verbos=1)
        if resp.status % 300 < 100:  # get link from redirection
            link = resp.getheader("Location")
            resp.close()
            errlog("ok:", link, verbos=1)
        elif resp.status == 200:  # get link from page
            resp.close()
            errlog("300 - sub page", verbos=1)
            self.get(url)
            urlanchor = self.driver.find_elements_by_css_selector(".urlworkaround>a")
            if len(urlanchor) == 0:
                return None
            link = urlanchor[0].get_attribute("href")
            errlog("ok:", link, verbos=1)
        # errlog(f'{resp.status}: {link}')
        return link

    def probe_moodle_resource_view_embed_mp4(self, url) -> Optional[re.Match]:
        resp = probe_http_head(url, self.known_cookies, {"User-Agent": UniVRBot.FAKE_USERAGENT})
        if resp.status != 200:
            # errlog('not 200')
            resp.close()
            return None

        content_type = resp.getheader("Content-Type")
        resp.close()
        # errlog(content_type)
        if not content_type.startswith("text/html"):
            return None
        resp2 = probe_http_get(url, self.known_cookies, {"User-Agent": UniVRBot.FAKE_USERAGENT})
        assert resp2.status == 200, "HEAD request got 200 but GET got different"
        data = gzip_decompress_or_raw(resp2.read()).decode()
        resp2.close()
        linkmatch = UniVRBot.RGX_MOODLE_MP4.search(data)
        # errlog('almost there')
        return linkmatch

    def load_moodle_course_page(self, course_id):
        d = self.driver
        self.get(UniVRBot.MOODLE_COURSE_PAGE_FMT.format(course_id))

        cond_header = lambda dr: dr.find_elements_by_tag_name("h1")
        # lambda dr: dr.title.startswith("Course: ")
        # Unsubscribed and "Enroll me" button
        # lambda dr: dr.find_element_by_id("id_submitbutton")
        # Unsubscribed and "Continue" button
        # lambda dr: dr.find_element_by_class_name("continuebutton")

        def course_loaded(dr):
            errlog("PanoptoFromCourse:", dr.title, verbos=2)
            return cond_header(dr)

        WebDriverWait(d, timeout=5).until(course_loaded)
        last_breadcrumb = d.find_elements_by_class_name("breadcrumb-item")[-1]
        if "enrol" in last_breadcrumb.text.lower():
            raise UnsubscribedError()

    def get_panopto_ids_from_course_id(self, course_id) -> List[str]:
        """Finds all panopto links by scraping a specific Moodle course page."""
        # pylint: disable=too-many-locals
        d = self.driver
        self.load_moodle_course_page(course_id)
        UniVRBot.wait_for_panopto_block(d)
        ret = []
        discover = []
        log()

        def stats():
            log(end="", erase_prev=True)
            l = len(ret)
            num = ""
            if l > 0:
                num = f" ({l})"
            log("Searching for lessons...", num, sep="")

        stats()

        # get all possible anchors
        errlog("[searching anchors]", verbos=1)
        anchors = d.find_elements_by_tag_name("a")
        for a in anchors:
            try:
                link = a.get_attribute("href")
                errlog("link:", link, verbos=1)
                if link is None:
                    continue
                if link.startswith(UniVRBot.PANOPTO_VIEWER_PAGE):
                    errlog("link is panopto", verbos=1)
                    lid = UniVRBot.RGX_ID.findall(link)
                    if len(lid) == 0:
                        continue
                    ret.append(lid[0])
                    stats()
                elif link.startswith(UniVRBot.MOODLE_URL_VIEW):
                    errlog("link is moodle url view", verbos=1)
                    discover.append(link)
            except StaleElementReferenceException:
                pass
            errlog(verbos=1)

        # explore moodle urls
        errlog("[exploring moodle urls]", verbos=1)
        for url in discover:
            # errlog(url)
            link = self.probe_moodle_url_view(url)
            if link is not None and link.startswith(UniVRBot.PANOPTO_VIEWER_PAGE):
                lid = UniVRBot.RGX_ID.findall(link)
                if len(lid) > 0:
                    ret.append(lid[0])
                    stats()
                errlog("lid:", lid, verbos=1)
            errlog(verbos=1)

        # take unique, remove panopto tutorial
        ret = set(ret)
        ret = list(filter(lambda x: x != UniVRBot.PANOPTO_TUTORIAL_ID, ret))
        log(end="", erase_prev=True)
        return ret

    """
    def get_panopto_lesson_comments(self, lesson_id) -> List[Comment]:
        return []

        # disabled, but working
        params = {
            "id": lesson_id,
            "type": "comments",
            "deliveryRelative": "true",
            "refreshAuthCookie": "true",
            "responseType": "json",
        }
        resp = probe_http_post(
            UniVRBot.PANOPTO_VIEW_RESULTS,
            urlencode(params),
            self.known_cookies,
            mergedict(
                UniVRBot.POST_REQ_HEADERS,
                {
                    "User-Agent": UniVRBot.FAKE_USERAGENT,
                    "Accept": "application/json",
                    "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
                },
            ),
        )
        assert resp.status == 200, "Response is not successful"
        data = gzip_decompress_or_raw(resp.read()).decode()
        resp.close()
        try:
            info = json.loads(data)
            iserror = info.get("ErrorMessage")
            if iserror:
                # errlog("Panopto reports an error:")
                # with logger:
                    # errlog(iserror, pointer=True)
                errcode = info.get("ErrorCode")
                if errcode == 2:
                    raise PermissionError(iserror)
                raise RuntimeError(iserror)
            events = info.get("Events")
            if events is None:
                errlog(info)
                raise RuntimeError("Data does not contain events info!")
        except json.JSONDecodeError as err:
            errlog()
            errlog("Not a JSON!")
            raise err

        def to_date(txt):
            if txt is None:
                return None
            m = UniVRBot.RGX_DATE.match(txt)
            if m:
                return m[1]
            return None

        comments = [
            Comment(
                user=e["UserDisplayName"],
                time=e["Time"],
                text=e["Data"],
                creation=to_date(e["CreationDateTime"]),
            )
            for e in events
            if e["EventTargetType"] == "Comments"
        ]

        return comments
    """

    def get_panopto_lesson_data(self, lesson_id) -> Optional[Lesson]:
        # pylint: disable=too-many-locals
        params = {
            "deliveryId": lesson_id,
            "invocationId": "",
            "isLiveNotes": "false",
            "refreshAuthCookie": "true",
            "isActiveBroadcast": "false",
            "isEditing": "false",
            "isKollectiveAgentInstalled": "false",
            "isEmbed": "false",
            "responseType": "json",
        }
        resp = probe_http_post(
            UniVRBot.PANOPTO_DELIVERYINFO,
            urlencode(params),
            self.known_cookies,
            mergedict(
                UniVRBot.POST_REQ_HEADERS,
                {
                    "User-Agent": UniVRBot.FAKE_USERAGENT,
                    "Accept": "application/json",
                    "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
                },
            ),
        )
        assert resp.status == 200, "Response is not successful"
        data = gzip_decompress_or_raw(resp.read()).decode()
        resp.close()
        try:
            info = json.loads(data)
            iserror = info.get("ErrorMessage")
            if iserror:
                # errlog("Panopto reports an error:")
                # with logger:
                # errlog(iserror, pointer=True)
                errcode = info.get("ErrorCode")
                lwr = iserror.lower()
                if errcode == 2 or "unauthorized access" in lwr or "this session isn't available" in lwr:
                    # return None
                    raise PermissionError(iserror)
                raise RuntimeError(iserror)
            delivery = info.get("Delivery")
            if delivery is None:
                errlog(info)
                raise RuntimeError("Data does not contain delivery info!")
        except json.JSONDecodeError as err:
            errlog()
            errlog("Not a JSON!")
            raise err

        delivery_domain = urlparse(delivery["Streams"][0]["StreamUrl"]).netloc
        session_id = delivery["SessionPublicID"]
        # ALERT Panopto uses BOTH Windows-epoch and Unix-epoch timestamps
        windowsdate = delivery["SessionStartTime"]
        if windowsdate is not None:
            windowsdate = win_to_unix_datetime(windowsdate).astimezone(None).replace(tzinfo=None)
        unixdate = None
        timestamps = delivery["Timestamps"]
        if len(timestamps) > 0:
            ts0 = timestamps[0]
            unixdate = UniVRBot.RGX_DATE.match(ts0["CreationDateTime"])
            if unixdate:
                unixdate = float(unixdate[1]) / 1000 - ts0["Time"]
                unixdate = dt.datetime.utcfromtimestamp(unixdate).astimezone(None).replace(tzinfo=None)

        nint = lambda t: int(t) if t is not None else None
        ntime = lambda t: int(round(t, 3) * 1000)  # do not use floats (milliseconds)
        lesson = Lesson(
            id=lesson_id,
            is_raw=False,
            name=delivery["SessionName"],
            authors=[a["DisplayName"] for a in delivery["Contributors"]],
            date=windowsdate or unixdate,
            duration=ntime(delivery["Duration"]),
            streams=[
                Stream(
                    url=t["StreamUrl"],  # doesn't need cookies
                    name=t["Name"],
                    tag=t["Tag"],
                    offset=0,
                    segments=[
                        Segment(
                            start=ntime(s["Start"]),
                            end=ntime(s["End"]),
                            map_to=ntime(s["RelativeStart"]),
                        )
                        for s in (t["RelativeSegments"] or [])
                    ],
                )
                for t in delivery["Streams"]
            ],
            # don't expect timestamps to be sorted by sequence number,
            # you may find 1->2->0->3
            timestamps=[
                Timestamp(
                    kind=t["EventTargetType"],  # 'ObjectVideo' == not a slide
                    isslide=t["EventTargetType"] != "ObjectVideo" and t["EventTargetType"] != "Primary",
                    number=nint(t["ObjectSequenceNumber"]),  # valid if >0
                    caption=t["Caption"],
                    content=t["Data"],
                    time=ntime(t["Time"]),
                    thumburl=UniVRBot.PANOPTO_THUMB_URL.format(
                        delivery_domain,
                        session_id,
                        t["ObjectPublicIdentifier"],
                        t["ObjectSequenceNumber"],
                    ),  # doesn't need cookies
                    slideurl=UniVRBot.PANOPTO_SLIDE_URL.format(
                        delivery_domain,
                        session_id,
                        t["ObjectPublicIdentifier"],
                        t["ObjectSequenceNumber"],
                    ),  # doesn't need cookies Nor any identification header
                )
                for t in timestamps
            ],
        )
        lesson.hint = LessonHint.get_new_hint(lesson)
        for st in lesson.streams:
            seg = st.segments
            if len(seg) == 1:
                g = seg[0]
                st.offset = g.map_to - g.start
        # lesson.comments = self.get_panopto_lesson_comments(lesson_id)
        assign_better_date_and_name(lesson)

        return lesson

    def __discover_raw_lessons(self) -> List[RawEntry]:
        d = self.driver
        found = []

        # get content page blocks
        blocks = d.find_elements_by_class_name("content")
        for block in blocks:
            # get block header text
            h3name = ""
            try:
                h3 = block.find_elements_by_tag_name("h3")
                if len(h3) > 0:
                    h3name = h3[0].text
            except StaleElementReferenceException:
                h3name = ""
            # errlog("h3:", h3name)
            # get labels and links
            try:
                lastlabel = ""
                elements = block.find_elements_by_css_selector(".label, section a")
                for e in elements:
                    tag = str(e.tag_name).lower()
                    # errlog("tag:", tag)
                    if tag == "li":  # label
                        lastlabel = e.text.strip().strip("\n")

                    elif tag == "a":  # link
                        link = e.get_attribute("href")
                        if link is None or not link.startswith(UniVRBot.MOODLE_RESOURCE_VIEW):
                            # errlog('not resource:', link)
                            continue
                        # errlog("link:", link)
                        img = e.find_elements_by_tag_name("img")
                        if len(img) == 0:
                            # errlog('no sub images:', link)
                            continue
                        imgsrc = img[0].get_attribute("src")
                        if len(UniVRBot.RGX_MPEG_ICO.findall(imgsrc)) == 0:
                            # errlog('no sub mpeg images:', link)
                            continue
                        # errlog('found:', link)
                        title = e.text.split("\n")[0]
                        # errlog("title:", title)
                        entry = RawEntry(url=link, title=title, label=lastlabel, header=h3name)
                        found.append(entry)
            except StaleElementReferenceException:
                pass

        return found

    def get_all_raw_lessons_data(self, course_id) -> List[Lesson]:
        self.load_moodle_course_page(course_id)

        ret = []
        log()

        def stats():
            log(end="", erase_prev=True)
            l = len(ret)
            num = ""
            if l > 0:
                num = f" ({l})"
            log("Searching for raw lessons...", num, sep="")

        stats()

        found = self.__discover_raw_lessons()
        found = UniVRBot.__assign_raw_lessons_names(found)

        # explore moodle urls, take unique
        for entry in found:
            m = self.probe_moodle_resource_view_embed_mp4(entry.url)
            if m is not None:
                url = m[0]  # fixURL(m[0])
                errlog("raw url:", url, verbos=1)

                # split at protocol commas ("https://...") and requote url
                # commas = url.find(":") + 1
                # url = url[:commas] + quote(url[commas:])
                resp = probe_http_head(url, self.known_cookies, {"User-Agent": UniVRBot.FAKE_USERAGENT})
                assert resp.status == 200, f"Url problem in raw lesson: {url}"
                last_modified = resp.getheader("Last-Modified")
                # errlog(last_modified)
                resp.close()
                luuid = url.encode()
                luuid = bytes.fromhex(digest(luuid, digest_size=16))
                luuid = str(uuid.UUID(bytes=luuid))
                dtparse = lambda s: parsedate_to_datetime(s).astimezone(None).replace(tzinfo=None)
                nn = lambda v, f: f(v) if v is not None else None
                lesson = Lesson(
                    id=luuid,
                    is_raw=True,
                    name=entry.title,  # m[1],
                    date=nn(last_modified, dtparse),
                    hint=LessonHint.SIMPLE,
                    authors=[],
                    streams=[
                        Stream(
                            url=url,  # NEEDS cookies
                            name="",
                            tag="RAW",
                            offset=0,
                            segments=[],
                        )
                    ],
                    timestamps=[],
                )
                assign_better_date_and_name(lesson)
                ret.append(lesson)
                stats()
        log(end="", erase_prev=True)
        return ret

    @staticmethod
    def wait_for_panopto_block(driver) -> None:
        """Waits panopto block for loading, if present."""
        try:
            WebDriverWait(driver, timeout=1).until(
                lambda d: d.find_element_by_id("block-region-side-pre")
            )  # sidebar loading
        except TimeoutException:  # this course page does not even have a sidebar
            return
        if len(driver.find_elements_by_css_selector('section.block_panopto[data-block="panopto"]')) > 0:
            log("Waiting for Panopto block...")
            try:
                WebDriverWait(driver, timeout=2).until(
                    lambda d: d.find_element_by_id("loading-text")
                )  # panopto block loading
            except TimeoutException:  # probably already loaded
                pass
            WebDriverWait(driver, timeout=20).until(
                lambda d: d.find_element_by_css_selector("#block_panopto_content>div")
            )  # loading finished
            log(end="", erase_prev=True)
            sys.stdout.flush()

    @staticmethod
    def __assign_raw_lessons_names(found: List[RawEntry]) -> List[RawEntry]:
        headerkey = lambda en: en.header
        labelkey = lambda en: en.label
        dummykey = lambda en: ""
        prefixes = set()

        def group_equal(g2: List, f: Callable, r: Callable):
            nonlocal prefixes
            name_nover = None
            for e2 in g2:
                if f(e2) != "":
                    return  # lessons with different label
                m2 = UniVRBot.RGX_NAME_NO_VER.match(e2.title)
                if not m2:
                    return  # lessons with different title
                prefix2 = m2[1]
                prefixes.add(prefix2)
                if name_nover is None:
                    name_nover = prefix2
                elif prefix2 != name_nover:
                    return  # lessons with different title
            # lessons with very similar title
            for e2 in g2:
                e2.title = r(e2)

        # assumptions:
        #    - in case of multiple headers, lessons must have different titles
        #    - grouping by header, lessons must be under the same label
        for _1, g in group_by(found, headerkey):
            g = list(g)
            if len(g) < 2:
                e = g[0]
                e.title = max((e.title, e.header), key=len)
                continue
            group_equal(g, labelkey, headerkey)

        aslist = lambda t: (t[0], list(t[1]))
        labelgroups = list(map(aslist, group_by(found, labelkey)))
        for _1, g in labelgroups:
            if len(g) < 2:
                continue
            group_equal(g, dummykey, labelkey)

        for _1, g in labelgroups:
            e = g[0]
            # errlog(len(g), e.title, e.label)
            if len(g) != 1:
                continue
            m = UniVRBot.RGX_NAME_NO_VER.match(e.title)
            if not m:
                continue
            prefix = m[1]
            # errlog(prefix, prefixes)
            if prefix in prefixes:
                e.title = e.label

        return found
