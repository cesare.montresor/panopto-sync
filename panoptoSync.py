#!/usr/bin/env python3
# pylint: disable=bad-whitespace
# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring

import sys
from pathlib import Path
from typing import Optional
from logs import logger, log, errlog


from argparse import Namespace
ARGS: Namespace


def main(argv) -> Optional[int]:
    import args as module_args
    from core import sigterm_back_proc
    from utils import generate_credentials

    global ARGS

    parser = module_args.make_command_parser()
    if len(argv) == 1:
        parser.print_help(sys.stderr)
        return 1
    ARGS = parser.parse_args(argv[1:])
    logger.verbosity = ARGS.verbosity
    ARGS.idpath = Path(ARGS.idpath)

    actions = {
        module_args.ACT_GENID:  lambda: generate_credentials(ARGS.idpath),
        module_args.ACT_SHOWID: show_credentials,
        module_args.ACT_SYNC:   syncronize
    }

    try:
        return actions[ARGS.action]()
    except EOFError:
        sigterm_back_proc()
        errlog()
        errlog("EOF reached.")
        return 1
    except KeyboardInterrupt:
        sigterm_back_proc()
        errlog()
        errlog("Interrupt received.")
        return 1
    except BaseException as unk:
        sigterm_back_proc()
        raise unk


def show_credentials():
    from utils import read_credentials
    cred, _1 = read_credentials(ARGS.idpath)
    log()
    log("Username:", cred.username)
    log("Password:", cred.password)


def syncronize() -> Optional[int]:
    import sync
    if not ARGS.onlyrename and len(sync.SYNC_REQUIRED_SOFTWARE) > 0:
        errlog(
            "Unable to find some softwares.",
            "Please install and make them visible from the system path.",
            sep="\n",
        )
        with logger:
            errlog(", ".join(sync.SYNC_REQUIRED_SOFTWARE))
        return 1

    # arguments of sync parser
    ARGS.cookiespath = Path(ARGS.cookiespath)
    ARGS.syncdir = Path(ARGS.syncdir)
    if ARGS.onlyrename:
        ARGS.nosearch = True
        ARGS.alllessons = True

    sync.ARGS = ARGS
    with sync.Synchronizer() as s:
        if not s.launch():
            return 1


if __name__ == "__main__":
    sys.exit(main(sys.argv) or 0)
