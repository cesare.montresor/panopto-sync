# pylint: disable=bad-continuation
# pylint: disable=bad-whitespace
# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-few-public-methods
# pylint: disable=pointless-string-statement


import sys
import os
import shutil
import re
import math
import itertools
import json
import subprocess
from datetime import datetime, timezone
from pathlib import Path
from hashlib import blake2b
from getpass import getpass
from typing import List, Tuple, Sequence, Mapping, Callable, IO, Optional, Union, Any
from logs import log, dump_args


# BEGIN Text
def ask_non_empty(vardesc: str, prompt: str = None, inputfunc: Callable = None) -> str:

    if prompt is None:
        prompt = f"{vardesc}: "
    if inputfunc is None:
        inputfunc = lambda p: str(input(p))
    errempty = f"{vardesc} must not be empty."
    while True:
        log(prompt, end="")
        sys.stdout.flush()
        ret = inputfunc("")
        if ret != "":
            break
        log(errempty, erase_prev=True)
    return ret


def ask_same(
    match: str,
    vardesc: str,
    prompt: str = None,
    inputfunc: Callable = None,
    errnomatch: str = None,
) -> str:

    if prompt is None:
        prompt = f"Repeat {vardesc}: "
    if inputfunc is None:
        inputfunc = lambda p: str(input(p))
    if errnomatch is None:
        errnomatch = f"{vardesc}s does not match."
    while True:
        log(prompt, end="")
        sys.stdout.flush()
        ret = inputfunc("")
        if ret == match:
            break
        log(errnomatch, erase_prev=True)
    return ret


def ask_pass(vardesc: str, prompt: str = None) -> str:
    return ask_non_empty(vardesc=vardesc, prompt=prompt, inputfunc=getpass)


def ask_new_pass(vardesc: str, prompt: str = None, promptrepeat: str = None, errnomatch: str = None) -> str:

    ret = ask_non_empty(vardesc=vardesc, prompt=prompt, inputfunc=getpass)
    ask_same(
        match=ret,
        vardesc=vardesc,
        prompt=promptrepeat,
        inputfunc=getpass,
        errnomatch=errnomatch,
    )
    return ret


RGX_FORMAT_NAMES = re.compile(r"{([^:]*).*?}")


def format_default(fmt, kwdata, default="") -> str:
    func = lambda m: m[0] if (m[1] in kwdata and kwdata[m[1]] is not None) else default
    fmt = RGX_FORMAT_NAMES.sub(func, fmt)
    return fmt.format(**kwdata)


def digest(data: bytes, *args, **kwargs) -> str:
    return blake2b(data, *args, **kwargs).hexdigest()


# END


# BEGIN Numbers
def nice_time(totalsec: float, display: bool = False, display_ms: bool = False) -> str:
    hours, remainder = divmod(totalsec, 3600)
    minutes, seconds = divmod(remainder, 60)
    if not display:
        return "{:02d}:{:02d}:{:06.3f}".format(int(hours), int(minutes), seconds)
    if hours == 0:
        if minutes == 0:
            if display_ms and seconds < 10:
                return f"{seconds:5.3f}s"
            return f"{int(seconds):d}s"
        return "{:02d}:{:02d}".format(int(minutes), int(seconds))
    return "{:02d}:{:02d}:{:02d}".format(int(hours), int(minutes), int(seconds))


BYTES_SCALES = ("", "K", "M", "G", "T")


def nice_bytes(totalbytes: int) -> str:
    degree = min(int(math.log(abs(totalbytes), 1024) if totalbytes != 0 else 0), 3)
    scaled = totalbytes / (1024 ** degree)
    if degree == 0:
        return f"{int(scaled):d}B"
    return f"{scaled:.2f}{BYTES_SCALES[degree]}iB"


EPOCH_UNIX = datetime(1970, 1, 1, tzinfo=timezone.utc)
EPOCH_WIN = datetime(1601, 1, 1, tzinfo=timezone.utc)
EPOCH_DELTA = EPOCH_UNIX - EPOCH_WIN


def win_to_unix_datetime(seconds: float) -> datetime:
    unix = seconds - EPOCH_DELTA.total_seconds()
    return datetime.utcfromtimestamp(unix)


# END


# BEGIN Platform
def ensure_directory(d: Path) -> None:
    if not d.is_dir():
        if d.is_symlink() and not d.exists():
            raise FileNotFoundError(f"This symlink targets a non-existing path: {d}")
        if d.exists():
            raise FileExistsError(f"This path is already taken by a file: {d}")
        d.mkdir()


EasyProcessArgs = Sequence[Union[Tuple[Any], Tuple[Any, Optional[Any]]]]


def popen_easy(pargs: EasyProcessArgs, *args, **kwargs):
    pargs2 = []
    for t in pargs:
        if len(t) == 1:
            pargs2.append(str(t[0]))
        elif t[1] is not None:
            pargs2.append(str(t[0]))
            pargs2.append(str(t[1]))
    dump_args("POPEN", pargs2)
    return subprocess.Popen(pargs2, *args, **kwargs)


FFPROBE_FORMAT_STREAMS_ARGS = (
    ("ffprobe",),
    ("-show_format",),
    ("-show_streams",),
    ("-of", "json"),
)


def ffprobe_format_streams(finput, timeout: float = None):
    pargs2 = FFPROBE_FORMAT_STREAMS_ARGS + ((finput,),)
    proc = popen_easy(
        pargs2,
        stdin=subprocess.DEVNULL,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        bufsize=0,
        universal_newlines=True,
        shell=False,
    )
    stdout: IO = proc.stdout
    proc.wait(timeout)
    txt = stdout.read()
    # errlog(txt)
    if proc.returncode != 0:
        raise subprocess.SubprocessError(f"Process returned {proc.returncode}.\n\n{proc.stderr.read()}")
    return json.loads(txt)


BackgroundMultiprocesses: List = []


def sigterm_back_proc():
    for proc in BackgroundMultiprocesses:
        proc.terminate()
    BackgroundMultiprocesses.clear()


def missing_software(*softwares) -> List[str]:
    return list(filter(lambda s: shutil.which(s) is None, softwares))


def set_file_times(f, date):
    if date is None:
        date = datetime.now()
    if isinstance(date, datetime):
        date = date.timestamp()
    elif date is float:
        raise ValueError("File time must be a datetime or float.")
    os.utime(f, times=(date, date))


def simple_shred(filename: str):
    size0 = os.path.getsize(filename)
    for _1 in range(1, 5 + 1):
        size1 = size0
        with open(filename, mode="wb") as f:
            while size1 > 0:
                block = min(size1, 4096)
                f.write(os.urandom(block))
                size1 -= block
    os.remove(filename)


# END


# BEGIN Language
r"""
def waitOnError(func: Callable, *args, errType=BaseException, **kwargs) -> Any:
    try:
        return func(*args, **kwargs)
    except errType as error:
        traceback.print_exc()
        errlog()
        errlog('Waiting 1h...')
        time.sleep(60*60)
        exit(1)
"""


def mergedict(dict_a: Mapping, dict_b: Mapping) -> Mapping:
    def merge_or_update(k):
        a, b = dict_a.get(k), dict_b.get(k)
        if b is None:
            return a
        if isinstance(a, dict) and isinstance(b, dict):
            return mergedict(a, b)
        return b

    return {k: merge_or_update(k) for k in dict_a.keys() | dict_b.keys()}


def _recur_get(d, k):
    if isinstance(d, dict):
        return d.get(k)
    if 0 <= k < len(d):
        return d[k]
    return None


def recur_get(a, *path, default=None) -> Any:
    if len(path) > 0:
        if a is None:
            return default
        k = path[0]
        a = _recur_get(a, k)
        return recur_get(a, *path[1:], default=default)
    return a


def comparison_chain(seq: List):
    ll = len(seq)
    if ll < 2:
        return
    not_first = False
    old = None
    for e in seq:
        if not_first:
            yield old, e
        not_first = True
        old = e


def group_by(seq: List, kfunc: Callable):
    return itertools.groupby(sorted(seq, key=kfunc), kfunc)


def coalesce(d: Mapping, *keys, default=None):
    if len(keys) == 0:
        return default
    if keys[0] in d:
        return d[keys[0]]
    return coalesce(d, *keys[1:], default=default)


def linear_maps(args, *mappings):
    for m in mappings:
        args = m(args)
    return args


def thisarg(func):
    def wrapped(*args, **kwargs):
        return func(wrapped, *args, **kwargs)

    return wrapped


def pub_class_priv_attr(obj, name):
    c = obj.__class__
    pre = ("_" + c.__name__) if name.startswith("__") else ""
    return c.__dict__[pre + name]


def simple_class(fields: str, clsname: str = None):  # fields='f1  f2  f3  fn'
    spl = fields.split()
    lf = len(spl) - 1
    attr_err_msg = "This attribute is not supported."

    class SimpleClass:
        __FIELDS = spl

        def __init__(self, *args, **kwargs):
            cls_fields = SimpleClass.__FIELDS
            for f in cls_fields:
                self.__dict__[f] = None
            i = 0
            for a in args:
                if i > lf:
                    raise AttributeError("Too many fields.")
                self.__dict__[cls_fields[i]] = a
                i += 1
            for k, v in kwargs.items():
                assert k in cls_fields, attr_err_msg
                self.__dict__[k] = v

        def __setattr__(self, name, value):
            if name not in SimpleClass.__FIELDS:
                raise AttributeError(attr_err_msg)
            super().__setattr__(name, value)

        def __iter__(self):
            return iter(self.__dict__.items())

        def __repr__(self):
            return repr(self.__dict__)

    if clsname is not None:
        SimpleClass.__name__ = clsname
    return SimpleClass


def anon_object(**kwargs):
    # https://stackoverflow.com/a/24448351
    # create a new anonymous class derived from object (by default)
    # with kwargs dictionary, and instantiate it
    return type('', (), kwargs)()
# END


"""
def getNextLineIterator(
        ffile: IO,
        ffilter: Callable[[str], bool],
        attempts: int = 10,
        eofsleep: float = .2) -> Iterator[str]:
    eofs = 0
    while True:
        line = ffile.readline()
        if ffilter(line):
            eofs = 0
            yield line
            continue
        if not line:
            eofs += 1
            if eofs >= attempts:
                yield TimeoutError("Can't get next line.")
                eofs = 0
            time.sleep(eofsleep)
"""
