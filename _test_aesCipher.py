#!/usr/bin/env python3

from Crypto import Random
from aesCipher import AESCipher
import unittest


class TestCipher(unittest.TestCase):
    def set_up(self):
        self.rnd = Random.new()
        self.key = self.rnd.read(1024)  # 1KB
        self.plaindata = self.rnd.read(1024 ** 2)  # 1MB
        self.cipher = AESCipher(self.key)

    def test_1_encrypt_noerr(self):
        self.cipher.encrypt(self.plaindata)

    def test_2_decrypt_noerr(self):
        enc = self.cipher.encrypt(self.plaindata)
        data2 = self.cipher.decrypt(enc)
        assert data2 == self.plaindata, "Decrypted data differs from original"

    def test_3_decrypt_err(self):
        enc = self.cipher.encrypt(self.plaindata)
        try:
            dec = AESCipher(self.rnd.read(1024)).decrypt(enc)
            if dec != self.plaindata:
                self.fail("No exception raised.")
            else:
                self.fail("Successfully decrypted!?")
        except ValueError as e:
            if "Invalid key." not in e.args:
                self.fail("Unknown exception.")


if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestCipher))
    runner = unittest.TextTestRunner(verbosity=2, failfast=True)
    runner.run(suite)
