import gzip
import http.client
import http.cookiejar
import io
import itertools
import time
from typing import List, Mapping, Union, IO, Iterable
from urllib import parse as ulp


Cookies = List[Mapping[str, str]]  # Selenium-compatible
# format: [{'name':n, 'value':v, 'domain':d, 'path':p, 'expiry':e, 'secure':s}, ]

Body = Union[bytes, IO, Iterable[bytes], str, None]


def get_relevant_cookies(cookies: Cookies, domain: str, path: str) -> Cookies:
    f = lambda c: domain.endswith(c["domain"]) and path.startswith(c["path"])
    return list(filter(f, cookies))


def dump_cookies_mozilla_format(cookies: Cookies, outfile: str):
    jar = http.cookiejar.MozillaCookieJar(outfile)
    for c in cookies:
        g = lambda k: c.get(k, "") or ""
        d = g("domain")
        mc = http.cookiejar.Cookie(
            0,
            g("name"),
            g("value"),
            None,
            False,
            d,
            d != "",
            d.startswith("."),
            g("path"),
            False,
            g("secure") == True,
            int(float(g("expiry") or 0)) or None,
            False,
            None,
            None,
            {},
        )
        jar.set_cookie(mc)
    jar.save(outfile, True, True)


def get_expired_cookie(timestamp, cookie) -> bool:
    exp = cookie.get("expiry")
    return exp is not None and timestamp >= int(exp)


def keep_valid_cookies(cookies):
    now = time.time()
    for co in list(filter(lambda c: get_expired_cookie(now, c), cookies)):
        cookies.remove(co)
    ks = lambda c: (c["domain"], c["path"], c["name"])
    kc = list(sorted(cookies, key=ks))
    for _k, g in itertools.groupby(kc, ks):
        g = list(g)
        for co in g[:-1]:  # except last
            cookies.remove(co)
        last = g[-1:][0]  # last
        if last.get("new"):
            del last["new"]


def merge_newer_cookies(older, newer) -> None:
    for co in newer:
        co["new"] = True
        older.append(co)
    ks = lambda c: (c["domain"], c["path"], c["name"])
    kc = sorted(older, key=ks)
    for _k, g in itertools.groupby(kc, ks):
        lc = list(g)  # list cookies
        if len(lc) > 1:
            for old in list(lc):
                if not old.get("new"):
                    older.remove(old)
                    lc.remove(old)
        newer = lc[0]
        if newer.get("new"):
            del newer["new"]


"""
def fix_url(url: str) -> str:
    parts = list(ulp.urlparse(url))
    parts[2] = ulp.quote(parts[2])
    return ulp.urlunparse(parts)
"""


def probe_http(
    method: str,
    url: str,
    cookies: Cookies = None,
    other: Mapping[str, str] = None,
    body: Body = None,
) -> http.client.HTTPResponse:
    urlparts = ulp.urlparse(url)
    conn = http.client.HTTPSConnection(urlparts.netloc, timeout=30)
    header = {k: v for k, v in (other or {}).items()}
    header["Host"] = urlparts.netloc
    send_body = body is not None
    if send_body:
        header["Content-Length"] = str(len(body))
    if cookies is not None:
        cookies = get_relevant_cookies(cookies, urlparts.netloc, urlparts.path)
        header["Cookie"] = "; ".join(map(lambda c: "=".join([c["name"], c["value"]]), cookies))
    loc = urlparts.path + ("?" + urlparts.query if len(urlparts.query) > 0 else "")
    # errlog(method, urlparts.netloc, loc, '', header, '', body, '', sep='\n')
    if send_body:
        conn.request(method, loc, body, headers=header)
    else:
        conn.request(method, loc, headers=header)
    resp = conn.getresponse()
    return resp


def probe_http_head(url: str, cookies: Cookies = None, other: Mapping[str, str] = None) -> http.client.HTTPResponse:
    return probe_http("HEAD", url, cookies, other)


def probe_http_get(url: str, cookies: Cookies = None, other: Mapping[str, str] = None) -> http.client.HTTPResponse:
    return probe_http("GET", url, cookies, other)


def probe_http_post(
    url: str, body: Body, cookies: Cookies = None, other: Mapping[str, str] = None
) -> http.client.HTTPResponse:
    return probe_http("POST", url, cookies, other, body or b"")


def gzip_decompress_or_raw(data: bytes) -> bytes:
    if data[:2] != b"\x1f\x8b":  # gzip magic
        return data
    stream = io.BytesIO(data)
    with gzip.open(stream) as g:
        ret = g.read()
    return ret
