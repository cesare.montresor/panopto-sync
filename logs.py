import shlex
import sys
import os
from typing import List
import colorama
from colorama import ansi  # noqa (import from here to init library)


colorama.init()


# https://stackoverflow.com/a/10455937
if os.name == 'nt':
    import ctypes
    class _CursorInfo(ctypes.Structure):
        _fields_ = [
            ("size", ctypes.c_int),
            ("visible", ctypes.c_byte)
        ]
    CI = _CursorInfo()
    HANDLE = ctypes.windll.kernel32.GetStdHandle(-11)

def hide_cursor(stream=sys.stdout):
    if os.name == 'nt':
        ctypes.windll.kernel32.GetConsoleCursorInfo(HANDLE, ctypes.byref(CI))
        CI.visible = False
        ctypes.windll.kernel32.SetConsoleCursorInfo(HANDLE, ctypes.byref(CI))
    elif os.name == 'posix':
        stream.write("\033[?25l")
        stream.flush()

def show_cursor(stream=sys.stdout):
    if os.name == 'nt':
        ctypes.windll.kernel32.GetConsoleCursorInfo(HANDLE, ctypes.byref(CI))
        CI.visible = True
        ctypes.windll.kernel32.SetConsoleCursorInfo(HANDLE, ctypes.byref(CI))
    elif os.name == 'posix':
        stream.write("\033[?25h")
        stream.flush()


class Log:
    def __init__(self, file=sys.stdout, **kwargs):
        self.verbosity = 0  # base verbosity level
        self.level = 0
        self.delta = 2
        self.file = file
        self.print = print
        self.printargs = kwargs

    def __enter__(self):
        self.level += 1
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.level = max(0, self.level - 1)

    def reset_level(self):
        self.level = 0

    @staticmethod
    def get_pad(level, delta) -> str:
        if level < 1:
            return ""
        return " " * (level * delta)

    def log(
        self,
        *args,
        pointer: bool = False,
        verbos: int = 0,
        erase_prev: bool = False,
        **kwargs,
    ):
        if verbos > self.verbosity:
            return
        kw = {**self.printargs, **kwargs}
        kw2 = dict(file=self.file, end="")
        kw2.update(kw)
        if pointer:
            pad = Log.get_pad(self.level - 1, self.delta) + "> "
        else:
            pad = Log.get_pad(self.level, self.delta)
        if erase_prev:
            kwe = dict(file=self.file, sep="", end="")
            kwe.update(self.printargs)
            self.print(ansi.Cursor.UP(1), ansi.clear_line(2), **kwe)
            # self.print('§', **kwe)
        if not erase_prev or len(args) > 0:
            self.print(pad, **kw2)
            self.print(*args, **kw)
        self.file.flush()

    def errlog(self, *args, **kwargs):
        self.log(*args, file=sys.stderr, **kwargs)


logger: Log = Log()
log = logger.log
errlog = logger.errlog


def dump_args(prefix: str, args: List[str]):
    errlog(verbos=2)
    errlog(prefix, "> ", " ".join([shlex.quote(a) for a in args]), sep="", verbos=2)
