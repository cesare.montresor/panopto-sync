import argparse
import re


ACT_GENID = "genid"
ACT_SHOWID = "showid"
ACT_SYNC = "sync"


def make_command_parser():
    _mfc = lambda prog: argparse.RawTextHelpFormatter(prog, max_help_position=29)

    parser = argparse.ArgumentParser(  # noqa
        description="""\
Automatically downloads and synchronizes UniVR Panopto lessons.
Copyright (C) 2020 Microeinstein

To show additional arguments, specify an action (eg. sync -h)""",
        formatter_class=_mfc,
    )
    subparsers = parser.add_subparsers(metavar="ACTION", dest="action", prog=None, required=True)  # noqa

    parser.add_argument(
        "--idfile",
        "-i",
        metavar="path",
        dest="idpath",
        default="default.id",
        help="credentials file",
    )
    parser.add_argument(
        "--verbose",
        "-v",
        dest="verbosity",
        action="count",
        default=0,
        help="increase verbosity",
    )

    subparsers.add_parser(ACT_GENID, formatter_class=_mfc, help="create credentials file")
    subparsers.add_parser(ACT_SHOWID, formatter_class=_mfc, help="show credentials file content")
    snc = subparsers.add_parser(ACT_SYNC, formatter_class=_mfc, help="synchronize the lessons")

    snc.add_argument(
        "--cookies",
        "-c",
        metavar="path",
        dest="cookiespath",
        default="default.cookies",
        help="browser cookies database file",
    )
    snc.add_argument(
        "--syncdir",
        "-d",
        metavar="path",
        dest="syncdir",
        default="lessons",
        help="synchronization folder",
    )
    snc.add_argument(
        "--force-older",
        "-w",
        dest="forceolder",
        action="store_true",
        help="do not abort if using an older download algorithm",
    )
    snc.add_argument(
        "--all-lessons",
        "-a",
        dest="alllessons",
        action="store_true",
        help="also iterate on cached lessons",
    )
    snc.add_argument(
        "--skip-existing",
        "-s",
        dest="skipexisting",
        action="store_true",
        help="do not download a lesson if a video already exists",
    )
    snc.add_argument(
        "--no-search",
        "-n",
        dest="nosearch",
        action="store_true",
        help="do not search anything - use cache",
    )
    snc.add_argument(
        "--dry-run",
        "-t",
        dest="dryrun",
        action="store_true",
        help="do not save/download/change anything on disk",
    )

    knowledge_group = snc.add_mutually_exclusive_group()
    knowledge_group.add_argument(
        "--known-courses",
        "-k",
        dest="knowncourses",
        action="store_true",
        help="search new lessons only for known courses",
    )
    knowledge_group.add_argument(
        "--unknown-courses",
        "-u",
        dest="unknowncourses",
        action="store_true",
        help="search new lessons only for unknown courses",
    )
    knowledge_group.add_argument(
        "--courses",
        metavar="filter",
        dest="coursesfilters",
        nargs="+",
        type=lambda query: re.compile(query, re.IGNORECASE),
        help="only consider courses containing one of these filters (regex)",
    )

    download_group = snc.add_mutually_exclusive_group()
    download_group.add_argument(
        "--lazy-meta",
        "-l",
        dest="lazymeta",
        action="store_true",
        help="update metadata if and only if a lesson has no video",
    )
    download_group.add_argument(
        "--only-meta",
        "-m",
        dest="onlymeta",
        action="store_true",
        help="do not download media streams; update metadata",
    )
    download_group.add_argument(
        "--only-rename",
        "-r",
        dest="onlyrename",
        action="store_true",
        help="download nothing; update filenames by cache content (implies -na)",
    )

    return parser
