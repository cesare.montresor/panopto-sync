# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring

import json
from pathlib import Path
from typing import Tuple, Any, Dict
from core import ask_non_empty, ask_pass, ask_new_pass
from web import Cookies
from logs import log
from bot import Credentials
from aesCipher import AESCipher

Cache = Dict[str, Dict[str, Any]]


def generate_credentials(p: Path) -> None:
    if p.exists():
        raise FileExistsError("This file already exists.")

    usr = ask_non_empty("Username")
    psw = ask_new_pass("Password")
    log()
    enc = ask_new_pass("File encryption password")

    cipher = AESCipher(enc.encode())
    data = cipher.encrypt_text("\n".join([usr, psw]))
    with p.open(mode="w", encoding="utf-8") as f:
        print(data, file=f)

    log("Success.")


def read_credentials(p: Path) -> Tuple[Credentials, str]:
    if not p.is_file():
        raise FileNotFoundError(f"Credentials file does not exists. ({p})")

    cred = Credentials()
    while True:
        credkey = ask_pass("Credentials password")
        cipher = AESCipher(credkey.encode())
        with p.open(mode="r", encoding="utf-8") as f:
            crypt = f.read()
        try:
            data = cipher.decrypt_text(crypt)
        except ValueError:
            log("Unable to decrypt credentials.", erase_prev=True)
            continue
        [cred.username, cred.password] = data.splitlines()
        break
    log("Accepted.")

    return cred, credkey


def read_cache(p: Path) -> Cache:
    def default(v):
        v.setdefault("options", {})
        v.setdefault("courses", {})
        return v

    if not p.is_file():
        return default({})
    ret: Cache
    with p.open(mode="r", encoding="utf-8") as f:
        ret = json.load(f)
    return default(ret)


def write_cache(p: Path, c: Cache) -> None:
    data = json.dumps(c, sort_keys=True, indent="\t")
    with p.open(mode="w", encoding="utf-8") as f:
        print(data, file=f)


def read_cookies(p: Path, enc: str) -> Cookies:
    if not p.is_file():
        return []
    ret: Cookies
    cipher = AESCipher(enc.encode())
    with p.open(mode="r", encoding="utf-8") as f:
        crypt = f.read()
        data = cipher.decrypt_text(crypt)  # no error check, it must work
        ret = json.loads(data)
    return ret


def write_cookies(p: Path, c: Cookies, enc: str) -> None:
    cipher = AESCipher(enc.encode())
    data = cipher.encrypt_text(json.dumps(c, separators=(",", ":")))
    with p.open(mode="w", encoding="utf-8") as f:
        print(data, file=f)
