# pylint: disable=bad-continuation
# pylint: disable=bad-whitespace
# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=too-many-instance-attributes
# pylint: disable=pointless-string-statement

from __future__ import unicode_literals  # youtube_dl

import io
import os
import re
import math
import shlex
import subprocess
from pathlib import Path
from typing import List, Tuple, Optional, IO
import ffmpeg
import youtube_dl
from caterpillar import caterpillar
from core import (
    nice_time,
    digest,
    comparison_chain,
    simple_class,
    EasyProcessArgs,
    popen_easy,
    ffprobe_format_streams,
    ensure_directory,
    simple_shred,
)
from web import Cookies, dump_cookies_mozilla_format, probe_http_get, gzip_decompress_or_raw
from logs import logger, errlog, dump_args
from classes import (
    Lesson,
    LessonHint,
    EnterExit,
    ErrorLogger,
    ProgressReporter,
)
from progress_bar import CustomMixedBar

Part = simple_class("gap from_a from_b to_a to_b", "Part")


# noinspection PyAttributeOutsideInit
class LessonDownloader:
    """Designed to be single-use; use with [with-as] statement."""

    RGX_BAD_CHAPTER_CAPT = re.compile(r"^[0-9]+$|\.\.\.$")
    RGX_BETTER_CHAPTER_CAPT = re.compile(r"^(?:[^a-z]+)?((?: *(?!the |il )[a-z.]+){1,3})", re.IGNORECASE)
    SLIDES_EXT = ".jpg"
    VIDEOS_EXT = ".mp4"
    OUTPUT_EXT = ".mkv"
    VERSION = 1591551221
    DEFAULT_FFMPEG_LOGLEVEL = 8  # fatal

    def __init__(
        self,
        lesson: Lesson,
        destdir: Path,
        cookies: Cookies = None,
        dryrun: bool = False,
    ):
        self.lesson: Lesson = lesson
        self.destdir = destdir
        self.outfile = lesson.filename  # with extension
        self.cookies = cookies
        self.dryrun = dryrun
        self.ffmpeg_loglevel = str(LessonDownloader.DEFAULT_FFMPEG_LOGLEVEL + (logger.level * 10))
        self.temp_files: List[str]  # delete on failure: yes (unused)
        self.resources: List[str]  # delete on failure: no
        self.final_media_args: EasyProcessArgs
        self.chapters_file: Optional[str]
        self.slides_data: List[Tuple[str, float]]  # (filename, duration)
        self.slides_videos: List[str]
        self.mayHaveWebcam: bool
        # self.globalOffset: float
        self.oldcwd: str
        self.namehash: str

    def __enter__(self):
        self.load()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.unload()

    def _get_final_segments(self, stream):
        ld = self.lesson.duration
        ssort = sorted(stream.segments, key=lambda segm: segm.map_to)
        ff = lambda part: part.to_a
        bounds = lambda n: min(max(0, n), ld)
        parts = []
        gaps = []
        rem = []
        # errlog(ssort)

        # get all parts
        # (for each part there must be a gap)
        for s in ssort:
            from_dur = s.end - s.start
            # from_a = s.start - max(-s.map_to, 0)
            # from_b = s.end - max(s.map_to + from_dur - ld, 0)
            if from_dur <= 0 or s.start == s.map_to:
                continue
            to_a = bounds(s.map_to)
            to_b = bounds(to_a + from_dur)
            if to_b - to_a <= 0 or s.start == to_a:
                continue
            parts.append(Part(False, s.start, s.end, to_a, to_b))
            gaps.append(Part(True, s.start, s.end, s.start, s.end))
        if len(parts) == 0:
            return parts
        parts.sort(key=ff)
        gaps.sort(key=ff)
        # errlog(parts)
        # errlog(gaps)
        # errlog()

        # overlap parts, fuse gaps
        for p1, p2 in list(comparison_chain(parts)):
            # errlog('>', p1, p2)
            diff = p1.to_b - p2.to_a
            # errlog('>', diff)
            if diff > 0:
                p1.to_b -= diff
                p1.from_b -= diff
                if p1.to_b - p1.to_a <= 0:
                    parts.remove(p1)
        for g1, g2 in list(comparison_chain(gaps)):
            diff = g1.to_b - g2.to_a
            if diff > 0:
                g2.to_a = g1.to_a
                gaps.remove(g1)
        # errlog(parts)
        # errlog(gaps)
        # errlog()

        # overlap parts on gaps
        for p in parts:
            for g in gaps:
                if p.to_a <= g.to_a <= p.to_b:
                    g.to_a = p.to_b
                if p.to_a <= g.to_b <= p.to_b:
                    g.to_b = p.to_a
                if g.to_b - g.to_a <= 0:
                    rem.append(g)
            for g in rem:
                gaps.remove(g)
            rem.clear()
        parts.sort(key=ff)
        gaps.sort(key=ff)
        # errlog(parts)
        # errlog(gaps)
        # errlog()

        # merge all together
        parts += gaps
        parts.sort(key=ff)

        # add remaining parts
        i = 0
        for p1, p2 in list(comparison_chain(parts)):
            diff = p2.to_a - p1.to_b
            if diff > 0:
                parts.insert(i + 1, Part(False, p1.to_b, p2.to_a, p1.to_b, p2.to_a))
            i += 1
        t0, tlen = min(parts[0].to_a, gaps[0].to_a), max(parts[-1].to_b, gaps[-1].to_b)
        if t0 > 0:
            parts.insert(0, Part(False, 0, t0, 0, t0))
        if tlen < ld:
            parts.append(Part(False, tlen, ld, tlen, ld))
        parts.sort(key=ff)
        # errlog(parts)
        # errlog(gaps)

        return parts

    def _make_matroska(self):
        # go = int(self.globalOffset * 1000)
        # https://mkvtoolnix.download/doc/mkvmerge.html
        mkvargs = [
            ("--title", self.lesson.name),
            ("--chapters", self.chapters_file),
            ("-o", self.outfile),
        ] + self.final_media_args
        LessonDownloader.build_matroska(mkvargs, self.dryrun)

    def _slides_to_video(self):
        # pylint: disable=too-many-locals
        # ldur = self.lesson.duration
        squeeze_factor = 10.0
        # create concat file with durations
        concatfile = self.namehash + "_concat.txt"
        self.resources.add(concatfile)
        with (open(concatfile, "w") if not self.dryrun else io.StringIO()) as f:
            print("ffconcat version 1.0", file=f)
            for s in self.slides_data:
                slidefile = s[0] + LessonDownloader.SLIDES_EXT
                slidefile = shlex.quote(slidefile)
                t1 = ("file", slidefile)
                t2 = ("duration", s[1] / 1000 / squeeze_factor)
                print(*t1, file=f)
                errlog(*t1, verbos=3)
                print(*t2, file=f)
                errlog(*t2, verbos=3)
        # add final file in consideration
        finalfile = self.namehash + "_concat" + LessonDownloader.VIDEOS_EXT
        self.resources.add(finalfile)
        self.final_media_args.append(("--track-name", "0:SLIDES"))
        self.final_media_args.append(("-y", "0:0," + str(squeeze_factor)))
        self.final_media_args.append((finalfile,))
        if Path(finalfile).is_file():
            return
        # invoke
        with CustomMixedBar(message=CustomMixedBar.make_msg("#s2video")):
            fi = ffmpeg.input(
                concatfile,
                loglevel=self.ffmpeg_loglevel,
                f="concat",
                safe="0",
            )
            fo = fi.output(finalfile, vcodec="h264", tune="stillimage", r=squeeze_factor)
            if not self.dryrun:
                fo.run()

    def _download_slides(self):
        # pylint: disable=too-many-locals
        flt = lambda t: (t.isslide and t.time is not None and t.number is not None and t.number > 0)
        slides_ts = filter(flt, self.lesson.timestamps)
        slides_ts = sorted(slides_ts, key=lambda t: t.time)
        lensl = len(slides_ts)

        i = 0
        with CustomMixedBar(message=CustomMixedBar.make_msg("#slides")) as prog:
            prog.steps_mode_init(lensl)
            for s in slides_ts:
                out = "{}_slide{}".format(self.namehash, i)
                jpg = out + LessonDownloader.SLIDES_EXT
                self.resources.add(jpg)
                p = Path(jpg)

                if not p.exists():
                    resp = probe_http_get(s.slideurl)
                    assert resp.status == 200, "Response is not successful"
                    data = gzip_decompress_or_raw(resp.read())
                    resp.close()
                    if not self.dryrun:
                        with open(jpg, "wb") as f:
                            f.write(data)

                # ignore appearing time of first slide (it must be shown at start)
                from_time = s.time if i > 0 else 0
                if i + 1 < lensl:
                    next_time = slides_ts[i + 1].time
                else:
                    next_time = self.lesson.duration
                # ffmpeg may complain on duration=0
                duration = max(100, next_time - from_time)  # milliseconds
                self.slides_data.append((out, duration))
                i += 1
                prog.steps_hook()

    def _concat_segments(self, segments: List[str], i: int):
        concatfile = self.namehash + f"_concat{i}.txt"
        self.resources.add(concatfile)
        with (open(concatfile, "w") if not self.dryrun else io.StringIO()) as f:
            print("ffconcat version 1.0", file=f)
            for s in segments:
                print("file", shlex.quote(s), file=f)
        # add final file in consideration
        finalfile = self.namehash + f"_concat{i}" + LessonDownloader.VIDEOS_EXT
        self.resources.add(finalfile)
        self.final_media_args.append((finalfile,))
        if Path(finalfile).is_file():
            return
        # invoke
        # no need for progress bar (call from _prepareSegments)
        fi = ffmpeg.input(concatfile, loglevel=self.ffmpeg_loglevel, f="concat", safe="0")
        fo = fi.output(finalfile, c="copy", y=None)
        dump_args("FFMPEG", fo.get_args())
        if not self.dryrun:
            fo.run()

    def _prepare_segments(self, stream, strfile: str, i: int):
        # pylint: disable=too-many-locals
        parts = self._get_final_segments(stream)
        for p in parts:
            errlog(
                "gap" if p.gap else "cut",
                f"[{p.from_a}-{p.from_b}]\t-> [{p.to_a}-{p.to_b}]",
                verbos=1,
            )
        lenparts = len(parts)
        if lenparts == 0:
            self.final_media_args.append((strfile,))
            return

        silence = "{}_silence.aac".format(self.namehash)
        self.resources.add(silence)
        if not Path(silence).exists():
            fi = ffmpeg.input("anullsrc", loglevel=self.ffmpeg_loglevel, f="lavfi")
            fo = fi.output(silence, acodec="aac", to="1", y=None)
            dump_args("FFMPEG", fo.get_args())
            if not self.dryrun:
                fo.run()

        finfo = lambda info: info["codec_type"] == "video"
        if not self.dryrun:
            sinfo = list(filter(finfo, ffprobe_format_streams(strfile)["streams"]))[0]
            iwi, ihe = sinfo["width"], sinfo["height"]
        else:
            iwi, ihe = 1, 1

        def make_gap(segm_out: str, duration: int):
            gen = f"color=c=black:size={iwi}x{ihe}"
            fiv = ffmpeg.input(gen, loglevel=self.ffmpeg_loglevel, f="lavfi")
            fia = ffmpeg.input(silence, loglevel=self.ffmpeg_loglevel, stream_loop="-1")
            fout = ffmpeg.output(fiv, fia, segm_out, vcodec="h264", to=nice_time(duration / 1000), r="1", y=None)
            dump_args("FFMPEG", fout.get_args())
            if Path(segm_out).exists():
                return
            if not self.dryrun:
                fout.run()

        def cut_segment(segm_out: str, start: int, end: int):
            fin = ffmpeg.input(strfile, loglevel=self.ffmpeg_loglevel)
            fout = fin.output(
                segm_out,
                c="copy",
                ss=nice_time(start / 1000),
                to=nice_time(end / 1000),
                y=None,
            )
            dump_args("FFMPEG", fout.get_args())
            errlog(segm_out, start / 1000, end / 1000, verbos=3)
            if Path(segm_out).exists():
                return
            if not self.dryrun:
                fout.run()

        si = 0
        with CustomMixedBar(message=CustomMixedBar.make_msg("")) as prog:  # no message, see below
            prog.steps_mode_init(lenparts + 1)
            sgm = []
            for p in parts:
                dur = p.to_b - p.to_a
                if dur > 0:
                    sout = "{}_segment{}.{}.mp4".format(self.namehash, i, si)
                    self.resources.add(sout)
                    prog.message = CustomMixedBar.make_msg("#s:" + ("gap" if p.gap else "cut"))
                    if p.gap:
                        make_gap(sout, dur)
                    else:
                        cut_segment(sout, p.from_a, p.from_b)
                    sgm.append(sout)
                prog.steps_hook()
                si += 1
            prog.message = CustomMixedBar.make_msg("#s:concat")
            self._concat_segments(sgm, i)
            prog.steps_hook()

    def _prepare_streams(self):
        i = 0
        webcam = self.lesson.hint.has_standalone_webcam()
        video0 = self.lesson.hint.stream0_has_video()
        for s in self.lesson.streams:
            if i == 2 and self.lesson.hint == LessonHint.WEBCAM_SCREEN_MERGED:
                continue  # ignore merged stream
            wd = "{}_stream{}".format(self.namehash, i)
            out = wd + ".mp4"
            self.resources.add(out)
            if self.lesson.is_raw:
                LessonDownloader.ytdl_progress(s.url, out, self.cookies, self.dryrun)
            else:
                assert self.cookies is None, "Usage of cookies in HLS streams is currently not supported."
                LessonDownloader.hls_progress(s.url, wd, out, self.dryrun)
            so = s.offset  # already in milliseconds
            fma = (
                # first stream: make every substream non-default
                ("--default-track", "-1:false" if i == 0 else None),
                # first stream: if video exists, make audio as default
                ("--default-track", "1:true" if i == 0 and video0 else None),
                # (same, but if video does not exist)
                ("--default-track", "0:true" if i == 0 and not video0 else None),
                ("--track-name", "-1:" + s.tag),
                # normal offset
                ("-y", f"-1:{so}" if so != 0 else None),
                # hopefully enhance webcam sync (shift audio by +25ms)
                ("-y", "1:25" if i == 0 and webcam else None),
            )
            for a in fma:
                self.final_media_args.append(a)
            if len(s.segments) == 0:
                self.final_media_args.append((out,))
            else:
                self._prepare_segments(s, out, i)
            i += 1

    def _make_subtitles(self):
        pass

    def _make_chapters(self):
        fslide = lambda ts: ts.isslide
        slide_chapters = filter(fslide, self.lesson.timestamps)
        slide_chapters = sorted(slide_chapters, key=lambda ts: ts.time)
        lmts = len(slide_chapters)
        if lmts == 0:
            self.chapters_file = None
            return
        self.chapters_file = f"{self.namehash}_chapters.txt"
        self.resources.add(self.chapters_file)

        chap_num_fmt = f"0{int(math.log10(lmts))}d"
        with (open(self.chapters_file, mode="w", encoding="utf-8") if not self.dryrun else io.StringIO()) as f:
            i = 1
            for ch in slide_chapters:
                td = nice_time(ch.time)  # ( + self.globalOffset)
                capt = ch.caption
                if capt is not None:
                    capt = capt.splitlines()[0]
                    if LessonDownloader.RGX_BAD_CHAPTER_CAPT.search(capt):
                        capt2 = LessonDownloader.RGX_BETTER_CHAPTER_CAPT.search(ch.content or "")
                        if capt2:
                            capt = capt2[1].title()
                else:
                    capt = str(i)
                print(f"CHAPTER{i:{chap_num_fmt}}={td}", file=f)
                print(f"CHAPTER{i:{chap_num_fmt}}NAME={capt}", file=f)
                i += 1

    def load(self):
        self.temp_files = set()
        self.resources = set()
        self.final_media_args = []
        self.slides_data = []
        self.slides_videos = []
        uniquename = self.outfile + str(self.lesson.date)
        self.namehash = digest(uniquename.encode(), digest_size=12)
        self.oldcwd = os.getcwd()
        if not self.dryrun:
            ensure_directory(self.destdir)
            os.chdir(str(self.destdir))  # change dir

    def launch(self, keep_resources: bool = False):
        lens = len(self.lesson.streams)
        assert lens > 0, "Lesson does not contain any stream"

        self._make_chapters()
        self._make_subtitles()
        self._prepare_streams()

        if self.lesson.hint == LessonHint.AUDIO_SLIDES:
            self._download_slides()  # download slides,
            self._slides_to_video()  # convert every slide into a single video

        # WARNING
        # delete existing output if present
        existing = Path(self.outfile)
        if existing.exists():
            if existing.is_dir():
                self.cleanup()
                raise FileExistsError("Output path is blocked by a directory!")
            if not self.dryrun:
                existing.unlink()

        self._make_matroska()

        if not (self.dryrun or keep_resources):
            # delete resources
            for f in self.resources:
                os.remove(f)

    def cleanup(self):
        """delete temporary files"""
        if not self.dryrun:
            for f in self.temp_files:
                os.remove(f)

    def unload(self):
        self.cleanup()
        if not self.dryrun:
            os.chdir(self.oldcwd)  # restore dir
        del self.temp_files
        del self.final_media_args
        del self.chapters_file
        del self.slides_data
        del self.slides_videos

    @staticmethod
    def hls_progress(url: str, wd: str, out: str, dry: bool = False):
        # pylint: disable=unnecessary-lambda
        with CustomMixedBar(
            message=CustomMixedBar.make_msg("#hls"),
            message2=CustomMixedBar.make_msg("#hls:concat"),
        ) as prog:
            rep = ProgressReporter(prog.progress_hook)
            if not dry:
                caterpillar.increase_logging_verbosity(-24)
                if (
                    caterpillar.process_entry(
                        m3u8_url=url,
                        output=Path(out),
                        workdir=Path(wd),
                        jobs=min((os.cpu_count() or 4) * 4, 16),
                        exist_ok=True,
                        progress=False,
                        event_hooks=[rep.event_hook],
                    )
                    != 0
                ):
                    raise RuntimeError("Caterpillar download failed.")
            prog.progress_hook_finished()

    @staticmethod
    def ytdl_progress(url: str, out: str, cookies: Cookies = None, dry: bool = False):
        # pylint: disable=unnecessary-lambda
        cookiefile = digest(url.encode(), digest_size=12)

        def dump_cookies():
            if not dry and cookies is not None:
                dump_cookies_mozilla_format(cookies, cookiefile)

        def shred_cookies():
            if not dry and os.path.exists(cookiefile):
                simple_shred(cookiefile)

        with EnterExit(dump_cookies, shred_cookies):
            with CustomMixedBar(message=CustomMixedBar.make_msg("#yt-dl")) as prog:
                ydl_opts = {
                    "quiet": True,
                    "logger": ErrorLogger(),
                    "outtmpl": out,
                    "nooverwrites": True,
                    "hls_prefer_native": True,
                    "progress_hooks": [lambda d: prog.progress_hook(d)],
                }
                if cookies is not None:
                    ydl_opts["cookiefile"] = cookiefile
                if not dry:
                    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                        ydl.download([url])
                prog.progress_hook_finished()

    @staticmethod
    def build_matroska(mkvargs: list, dry: bool = False):
        mkvargs.insert(0, ("mkvmerge",))
        mergeproc = popen_easy(
            mkvargs,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            bufsize=0,
            universal_newlines=True,
            shell=False,
        )
        stdout: IO = mergeproc.stdout
        with CustomMixedBar(message=CustomMixedBar.make_msg("#matroska")) as prog:
            buffout = ""
            allout = ""
            if not dry:
                while mergeproc.poll() is None:
                    txt = stdout.read(3)
                    buffout += txt
                    allout += txt
                    end = prog.mkvmerge_progress(buffout)
                    if end is not None:
                        buffout = buffout[end:]
                if mergeproc.returncode != 0:
                    allout += stdout.read()
                    raise subprocess.SubprocessError(f"Process returned {mergeproc.returncode}.\n\n{allout}")
