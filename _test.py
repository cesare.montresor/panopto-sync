#!/usr/bin/env python3

import time
import base64
import unittest
import random as rnd
import datetime as dt
from pathlib import Path
from datetime import datetime
from core import (
    ensure_directory,
)
from extract_date import MONTHS, DAYS, find_known_date_format, assign_better_date_and_name
from progress_bar import CustomMixedBar
from web import probe_http_get, gzip_decompress_or_raw
from logs import logger
from utils import read_credentials, read_cookies
from bot import Lesson, Stream, Segment, UniVRBot
from download import LessonDownloader
from typing import Optional

logger.verbosity = 10  # arbitrary value
skip_all_ok = "Tested: all working"
skip_ok = "Tested: working"

bot: Optional[UniVRBot] = None


def load_bot():
    global bot
    idpath = Path("default.id")
    cookiespath = Path("default.cookies")
    cred, encpsw = read_credentials(idpath)
    cookies = read_cookies(cookiespath, encpsw)
    bot = UniVRBot(cred, cookies)


# load_bot()


@unittest.skip(skip_all_ok)
class TestGaps(unittest.TestCase):
    @unittest.skip(skip_ok)
    def test_1_stream_gaps1(self):
        segm = [
            Segment(map_to=2, start=5, end=8),  # 2->5
            # gap 2
            Segment(map_to=7, start=5, end=10),  # 7->12
        ]
        stre = Stream(segments=segm)
        lesson = Lesson(duration=20, streams=[stre], filename="out.mkv")
        ld = LessonDownloader(lesson, Path("whatever"))
        print()
        for p in ld._get_final_segments(lesson.streams[0]):
            print(
                "gap" if p.gap else "cut",
                f"[{p.from_a}-{p.from_b}]\t-> [{p.to_a}-{p.to_b}]",
            )

    @unittest.skip(skip_ok)
    def test_1_stream_gaps2(self):
        segm = [
            Segment(map_to=2, start=5, end=8),  # 2->5
            # overlap
            Segment(map_to=4, start=5, end=10),  # 4->9
            # gap 1
            # original
            # gap 1
            Segment(map_to=12, start=11, end=20),  # 12->20
        ]
        stre = Stream(segments=segm)
        lesson = Lesson(duration=20, streams=[stre], filename="out2.mkv")
        ld = LessonDownloader(lesson, Path("whatever"))
        print()
        for p in ld._get_final_segments(lesson.streams[0]):
            print(
                "gap" if p.gap else "cut",
                f"[{p.from_a}-{p.from_b}]\t-> [{p.to_a}-{p.to_b}]",
            )


@unittest.skip(skip_all_ok)
class TestDates(unittest.TestCase):
    @unittest.skip(skip_ok)
    def test_2_known_date_format1(self):
        print()
        for _1 in range(1, 100):
            txt = " ".join(
                (
                    rnd.choice(rnd.choice(DAYS))[:3] + ",",
                    rnd.choice(rnd.choice(MONTHS)[:3]),
                    str(rnd.randint(1, 28)),
                    "2020",
                    "at",
                    rnd.choice((".", ":", ";")).join((str(rnd.randint(1, 12)), str(rnd.randint(0, 59)))),
                    rnd.choice(("AM", "PM")),
                )
            )
            num, match, *t = find_known_date_format(txt)
            print(txt)
            print(num, *t)
            dt.datetime(*t)

    @unittest.skip(skip_ok)
    def test_2_known_date_format2(self):
        print()
        for _1 in range(1, 100):
            txt = " ".join(
                (
                    rnd.choice(rnd.choice(DAYS))[:3],
                    str(rnd.randint(1, 28)),
                    rnd.choice(rnd.choice(MONTHS))[:3],
                    "2020",
                    "at",
                    ".".join(
                        (
                            str(rnd.randint(0, 23)),
                            str(rnd.randint(0, 59)),
                            str(rnd.randint(0, 59)),
                        )
                    ),
                )
            )
            num, match, *t = find_known_date_format(txt)
            dt.datetime(*t)

    @unittest.skip(skip_ok)
    def test_2_known_date_format5(self):
        print()
        for _1 in range(1, 100):
            txt = " ".join(
                (
                    str(rnd.randint(1, 28)),
                    rnd.choice(rnd.choice(MONTHS))[:3],
                    "2020",
                )
            )
            num, match, *t = find_known_date_format(txt)
            t[3] = 0
            t[4] = 0
            # print(num, *t)
            dt.datetime(*t)

    @unittest.skip(skip_ok)
    def test_3_other_date_formats(self):
        titles_date = (
            "IMPRENDITORIA_Testimonianza_T2i_M.Brunelli-A.Previato_07.05.20_Zoom Meeting",
            "IMPRENDITORIA_Testimone 7 aprile 20_Luca Guarnieri_LIAISON OFFICE UNIVR-presentazione",
            "Lezione_14Martedì_qualcosa",  # -> 14/??
        )
        titles_nodate = (
            "Lezione 7.2 Sistemi a tempo continuo. Risposta libera",
            "Lezione_14_Venerdì_12_qualcosa",
        )
        for txt in titles_date:
            print(txt)
            num, match, *t = find_known_date_format(txt)
            print(num, *t)
        for txt in titles_nodate:
            print(txt)
            ret = find_known_date_format(txt)
            print(ret)
            assert ret is None

    # @unittest.skip(skip_ok)
    def test_4_full_name_date_fix(self):
        titles = (
            "IMPRENDITORIA_Testimone 7 aprile 20_Luca Guarnieri_LIAISON OFFICE UNIVR-presentazione",
            "Gio23.04.2020(IV) - Lab4 - slides14-19 durata26'58''",
            "Wed, Mar 11 2020 at 1:54 PM",
            "Lez whatever (II; Lun 30/03/20)",
            "Lez del 14/2//20",
            "Lez del 14 2 20",
            "Crittografia 2 Integrità 1",
            "Compilatori 31 Marzo",
            "Analisi Matematica 1 - 2020-11-19",
        )
        lesson = Lesson(date=datetime.now())
        for txt in titles:
            print(txt)
            lesson.name = txt
            assign_better_date_and_name(lesson)
            print("fixed:", lesson.name_fixed)
            print("date_name:", lesson.date_name)
            print("date_better:", lesson.date_better)
            print()


# @unittest.skip(skip_all_ok)
class TestOther(unittest.TestCase):
    # @unittest.skip(skip_ok)
    def test_progress_bar(self):
        with CustomMixedBar(message='loading') as prog:
            prog.steps_mode_init(10)
            for _1 in range(0, 10):
                time.sleep(.5)
                prog.steps_hook()


@unittest.skip(skip_all_ok)
class TestBot(unittest.TestCase):
    def set_up(self):
        pass

    def tear_down(self):  # noqa
        if bot.driver is not None:
            bot.quit()

    @unittest.skip(skip_ok)
    def test_0_launch(self):
        bot.launch()

    @unittest.skip(skip_ok)
    def test_1_login(self):
        bot.login_giasso()
        bot.login_panopto(skip_giasso=True)

    @unittest.skip(skip_ok)
    def test_2_get_panopto_ids(self):
        for lesson in bot.get_panopto_ids_from_course_id(2877):
            print(">", lesson)

    @unittest.skip("Manual requests logging no more necessary.")
    def test_3_load_lesson(self):
        bot.get("https://univr.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=3bebb456-697c-4fe9-8659-ab9600da2e38")

    @unittest.skip(skip_ok)
    def test_4_get_panopto_lesson_data(self):
        data = bot.get_panopto_lesson_data("c0895c0f-b49b-4f60-ad8d-ab7900eb1499")
        print(data)


@unittest.skip(skip_all_ok)
class TestMain(unittest.TestCase):
    def setUp(self):
        print()
        self.testdir = Path("testdir")
        ensure_directory(self.testdir)
        bot.launch()
        bot.login_giasso()
        bot.login_panopto(skip_giasso=True)

    def tearDown(self):
        bot.quit()
        print()

    @unittest.skip(skip_ok)
    def test_1_get_panopto_lesson_data1(self):
        data = bot.get_panopto_lesson_data("3fd372be-60a5-4636-8c7c-ab7600b24d3b")
        print(data.__dict__)

    @unittest.skip(skip_ok)
    def test_1_get_panopto_lesson_data2(self):
        data = bot.get_panopto_lesson_data("3bebb456-697c-4fe9-8659-ab9600da2e38")
        print(data.__dict__)

    @unittest.skip(skip_ok)
    def test_1_get_panopto_lesson_data3_slides(self):
        data = bot.get_panopto_lesson_data("f6bb7d26-164d-4ff2-aeab-ab7b008c15c9")
        ts21 = list(filter(lambda ts: ts.isslide and ts.number == 21, data.timestamps))[0]
        resp = probe_http_get(ts21.slideurl)
        assert resp.status == 200, "Response is not successful"
        slide21 = gzip_decompress_or_raw(resp.read())
        resp.close()
        print(base64.b64encode(slide21).decode())

    @unittest.skip(skip_ok)
    def test_2_download_lesson1(self):  # audio+screen
        data = bot.get_panopto_lesson_data("3fd372be-60a5-4636-8c7c-ab7600b24d3b")
        data.filename = "test2.1.mkv"
        with LessonDownloader(data, self.testdir) as d:
            d.launch(True)

    @unittest.skip(skip_ok)
    def test_2_download_lesson2(self):  # common lesson
        data = bot.get_panopto_lesson_data("3bebb456-697c-4fe9-8659-ab9600da2e38")
        data.filename = "test2.2.mkv"
        with LessonDownloader(data, self.testdir) as d:
            d.launch(True)

    @unittest.skip(skip_ok)
    def test_2_download_lesson3(self):  # audio+slides
        data = bot.get_panopto_lesson_data("c0895c0f-b49b-4f60-ad8d-ab7900eb1499")
        data.filename = "test2.3.mkv"
        with LessonDownloader(data, self.testdir) as d:
            d.launch(True)

    @unittest.skip(skip_ok)
    def test_2_download_lesson4(self):  # audio+slides
        data = bot.get_panopto_lesson_data("f6bb7d26-164d-4ff2-aeab-ab7b008c15c9")
        data.filename = "test2.4.mkv"
        with LessonDownloader(data, self.testdir) as d:
            d.launch(True)

    # @unittest.skip(skip_ok)
    def test_2_download_lesson5(self):  # weird offsets
        data = bot.get_panopto_lesson_data("280da7f8-54d8-45df-80f0-ab8e00ea8d43")
        data.filename = "test2.5.mkv"
        with LessonDownloader(data, self.testdir, None) as d:
            d.launch(True)

    @unittest.skip(skip_ok)
    def test_2_download_lesson6(self):  # 3 streams, offsets
        data = bot.get_panopto_lesson_data("341a8d03-7dde-442d-93fb-ab98008f63e5")
        data.filename = "test2.6.mkv"
        with LessonDownloader(data, self.testdir) as d:
            d.launch(True)


if __name__ == "__main__":
    suite = unittest.TestSuite()
    for test in (TestGaps, TestDates, TestOther, TestBot, TestMain):
        suite.addTest(unittest.makeSuite(test))
    runner = unittest.TextTestRunner(verbosity=2, failfast=True)
    runner.run(suite)
