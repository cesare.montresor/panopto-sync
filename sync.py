# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=too-many-instance-attributes

import re
import datetime as dt
from pathlib import Path
from urllib.parse import urlparse, parse_qs
from http.client import HTTPException
from typing import List, Tuple, Optional, Callable, Any
from selenium.common.exceptions import WebDriverException, TimeoutException

from core import (
    missing_software,
    ensure_directory,
    set_file_times,
    group_by,
)
from logs import logger, log, errlog
from extract_date import assign_better_date_and_name
from title_format import (
    BETTER_FILENAME_FMT,
    set_course_name_fields,
    pathify_lesson_name,
    pathify_course_name,
    format_lesson_filename,
)
from utils import read_credentials, read_cache, write_cache, read_cookies, write_cookies
from classes import Course, Lesson
from bot import UnsubscribedError, UniVRBot
from download import LessonDownloader

SYNC_REQUIRED_SOFTWARE = missing_software("firefox", "geckodriver", "ffmpeg", "ffprobe", "youtube-dl", "mkvmerge")


from argparse import Namespace
ARGS: Namespace  # value is set in module panoptoSync.py


class Synchronizer:
    """Use with [with-as] statement."""

    SKIP_LESSON_KEYS_W = """
            course courseid id
            streams timestamps
            date date_name date_better
            socialurl hint comments
            cached filename2
        """.split()  # do not write these keys
    SKIP_LESSON_KEYS_R = """
            course courseid id
            streams timestamps
            socialurl
        """.split()  # do not read these keys
    SKIP_COURSE_KEYS = """id name_clean""".split()
    LESSON_DATES = "date date_name date_better".split()

    def __init__(self):
        # pylint: disable=bad-whitespace
        self.cache = None
        self.bot: Optional[UniVRBot] = None
        self.num_failed_courses = None
        self.num_failed_lessons = None
        self.cred = None
        self.credkey = None
        self.cookies = None
        self.cachefile = None
        self.lff = None  # lesson file format

    def __enter__(self):
        self.load()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.unload()

    def _save_cookies(self):
        if not ARGS.dryrun:
            write_cookies(ARGS.cookiespath, self.cookies, self.credkey)

    def _save_cache(self):
        if not ARGS.dryrun:
            write_cache(self.cachefile, self.cache)

    def _cache_course(self, course: Course):
        """Map every course attribute in cache, except for some attributes"""
        c = self.cache.setdefault("courses", {})
        c[course.id] = {k: v for k, v in course if k not in Synchronizer.SKIP_COURSE_KEYS}
        return c[course.id]

    def _courses_from_cache(self) -> List[Course]:
        def convert(i, d):  # 1 course
            kw = {k: v for k, v in d.items()}
            kw["id"] = i
            return kw

        return [set_course_name_fields(Course(**convert(k, v))) for k, v in self.cache["courses"].items()]

    def _clean_empty_lessons(self):
        for _, c in self.cache["courses"].items():
            c["lessons"] = {k: v for k, v in c["lessons"].items() if len(v) > 0}

    @staticmethod
    def _cache_lesson(course: Course, lesson: Lesson):
        """Save the lesson in cache, except for some attributes"""
        d = {k: v for k, v in lesson if k not in Synchronizer.SKIP_LESSON_KEYS_W}

        def convdate(date: dt.datetime):
            if date is None:
                return None
            return date.timestamp()  # save in localtime

        for k in Synchronizer.LESSON_DATES:
            d[k] = convdate(lesson.__dict__[k])
        course.lessons[lesson.id] = d
        return d

    @staticmethod
    def _lesson_from_cache(course: Course, lesson_id: str) -> Lesson:
        d = {k: v for k, v in course.lessons[lesson_id].items() if k not in Synchronizer.SKIP_LESSON_KEYS_R}
        for k in Synchronizer.LESSON_DATES:  # read in localtime
            d[k] = dt.datetime.fromtimestamp(d[k]) if d[k] else None
        return Lesson(courseid=course.id, course=course.name, id=lesson_id, cached=True, **d)

    @staticmethod
    def _ensure_lesson_name(course: Course, course_dir: Path, lesson: Lesson, dry: bool) -> bool:
        filename2 = lesson.filename2
        p1 = Path(course_dir, filename2)
        if p1.exists():
            lesson.filename = filename2
            return True

        def rename_old_file(oldname: str) -> bool:
            if oldname is None:
                return False
            p0 = Path(course_dir, oldname)
            if oldname != filename2 and p0.is_file():
                if dry:  # renaming is performed from _handleLesson only
                    return True
                if not ARGS.dryrun:  # user argument
                    p0.rename(p1)
                log("[Renamed]")
                with logger:
                    log("from:", oldname)
                    log("  to:", filename2)
                log()
                lesson.filename = filename2
                Synchronizer._cache_lesson(course, lesson)
                return True
            return False

        on1 = pathify_lesson_name(lesson.name) + LessonDownloader.OUTPUT_EXT
        on2 = lesson.filename
        return rename_old_file(on1) or rename_old_file(on2)

    def _find_new_courses(self) -> List[Course]:
        return list(filter(lambda c: c.id not in self.cache["courses"], self.bot.get_all_courses()))

    def _find_new_panopto_lessons(self, course_id: str) -> List[str]:
        assert course_id in self.cache["courses"], "Cache does not contain this course"
        f = lambda c: c not in self.cache["courses"][course_id]["lessons"]
        return list(filter(f, self.bot.get_panopto_ids_from_course_id(course_id)))

    def _get_panopto_lesson_data(self, course: Course, lesson_id: str) -> Optional[Lesson]:
        # log('Getting data...')
        course.lessons.setdefault(lesson_id, {})  # add initial placeholder
        try:
            lesson = self.bot.get_panopto_lesson_data(lesson_id)
        except HTTPException:
            errlog("Unable to get data for lesson", f"{UniVRBot.PANOPTO_VIEWER_PAGE}?id={lesson_id}", pointer=True)
            self.num_failed_lessons += 1
            return None
        except PermissionError:
            errlog("Unauthorized access to lesson", f"{UniVRBot.PANOPTO_VIEWER_PAGE}?id={lesson_id}", pointer=True)
            self.num_failed_lessons += 1
            return None
        # log(end='', erase=True)
        return lesson


    def _handle_lesson(self, course: Course, course_dir: Path, lesson: Lesson):
        log(lesson.name, pointer=True)
        with logger:
            Synchronizer._ensure_lesson_name(course, course_dir, lesson, False)
            lesson.filename = lesson.filename2
            path = Path(course_dir, lesson.filename)

            if not (ARGS.onlymeta or ARGS.onlyrename):
                if not path.exists() or (ARGS.alllessons and not ARGS.skipexisting):
                    if path.is_dir():
                        errlog("Warning: the output path is blocked by a directory.")
                        log()
                        self.num_failed_lessons += 1
                        return
                    if len(lesson.streams) > 0:
                        c = self.bot.known_cookies if lesson.is_raw else None
                        with LessonDownloader(lesson, course_dir, c, ARGS.dryrun) as d:
                            d.launch()
                    else:
                        errlog("Warning: this lesson has no streams, ignoring...")
                        log()
                        self.num_failed_lessons += 1
                        return
                    log()

            # in case of failure, do not cache lesson (returned before)
            Synchronizer._cache_lesson(course, lesson)

            # set file access/modified times
            if path.is_file():
                if not ARGS.dryrun:
                    set_file_times(path, lesson.date_better)
            else:
                errlog("Warning: missing file.")
                self.num_failed_lessons += 1

    def _handle_lessons(self, course: Course, course_dir: Path, lessons: List[Lesson], recur=0):
        # first iteration:
        #   group by fixed name
        #   make filename2 from fixed name (see format lff)
        #   check for missing files
        #       fetch metadata again
        #       save to list
        # second iteration:
        #   regroup by fixed name
        #   remake filename2
        assert recur <= 1, "Too many recursions"
        namekey = lambda l: l.name_fixed
        datekey = lambda l: l.date_better
        missing = False
        for _1, g in group_by(lessons, namekey):
            grouped = sorted(g, key=datekey)
            duplicates = len(grouped) > 1
            num = 0
            # errlog(len(grouped))
            for lesson in grouped:
                num += 1
                filename2 = format_lesson_filename(self.lff, course, lesson, num if duplicates else 0)
                filename2 += LessonDownloader.OUTPUT_EXT
                lesson.filename2 = filename2
                # fn = lesson.filename
                is_downloaded = Synchronizer._ensure_lesson_name(course, course_dir, lesson, True)
                # errlog(downloaded, f'[{lesson.name_fixed}]', f'[{fn}]', f'[{filename2}]')
                if ARGS.lazymeta and lesson.cached and not is_downloaded and not lesson.is_raw:
                    index = lessons.index(lesson)
                    lesson = self._get_panopto_lesson_data(course, lesson.id)
                    # cached is None
                    lesson.filename2 = filename2
                    lessons[index] = lesson
                    missing = True
        if missing:
            return self._handle_lessons(course, course_dir, lessons, recur + 1)

        for lesson in sorted(lessons, key=datekey):
            self._handle_lesson(course, course_dir, lesson)

    def _handle_course(self, course: Course):
        log(course.name, pointer=True)
        # skip courses that certainly have 0 lessons
        if not ARGS.onlyrename and (course.skip or (course.year is not None and course.year < 2019)):
            return

        def retrieve_lesson(lid):
            cached = lid in course.lessons
            if ARGS.nosearch or cached:
                assert cached, "Lesson is not cached"
                l = Synchronizer._lesson_from_cache(course, lid)
                assign_better_date_and_name(l)
                return l
            return self._get_panopto_lesson_data(course, lid)

        with logger:
            pcn = pathify_course_name(course.name)
            course_dir = Path(ARGS.syncdir, pcn)
            old_failed = self.num_failed_lessons
            try:
                if ARGS.nosearch:
                    ids = course.lessons.keys()
                    lessons = list(map(retrieve_lesson, ids))
                else:
                    if ARGS.alllessons:
                        ids = self.bot.get_panopto_ids_from_course_id(course.id)
                    else:
                        ids = self._find_new_panopto_lessons(course.id)
                    lessons = list(filter(lambda l: l is not None, map(retrieve_lesson, ids)))
                    if course.search_raws:
                        lessons.extend(self.bot.get_all_raw_lessons_data(course.id))

                self._handle_lessons(course, course_dir, lessons)

                num_known_lessons = len(self.cache["courses"][course.id]["lessons"])
                if num_known_lessons > 0:
                    cur_failed = self.num_failed_lessons - old_failed
                    if not (ARGS.onlymeta or ARGS.onlyrename):
                        log(f"(completed {num_known_lessons - cur_failed}/{num_known_lessons})")
                else:
                    log("(no lessons)")

            except UnsubscribedError:
                errlog("Warning: not subscribed to this course.")
                self.num_failed_courses += 1

            log()

    def _main_sync(self):
        def common(to_title: Callable, get: Callable, cache: bool):
            log()
            to_title()

            with logger:
                courses = get()
                if len(courses) == 0:
                    log("(no new courses)")
                    return

                def handle():
                    if cache:
                        self._cache_course(course)
                    self._handle_course(course)

                if ARGS.coursesfilters is not None:
                    for course in courses:
                        for r in ARGS.coursesfilters:
                            if r.search(course.name) is not None:
                                handle()
                                break
                else:
                    for course in courses:
                        handle()

        if ARGS.nosearch:

            def title_func():
                if ARGS.onlyrename:
                    log("Renaming known lessons...")
                else:
                    log("Downloading all lessons for known courses...")

            common(title_func, self._courses_from_cache, False)

        else:
            numc = "specified" if len(ARGS.coursesfilters or []) > 0 else ""

            if not ARGS.unknowncourses:
                numl = "all" if ARGS.alllessons else "new"
                if not numc:
                    numc = " "
                else:
                    numc = f" {numc} "
                common(
                    lambda: log("Finding", numl, f"lessons for known{numc}courses..."),
                    self._courses_from_cache,
                    False,
                )

            if not ARGS.knowncourses:
                common(lambda: log("Finding new courses..."), self._find_new_courses, True)

    def _check_version(self) -> bool:
        cached_courses = len(self.cache["courses"])
        if cached_courses > 0:
            last_dv = self.cache["options"].get("downloaderVersion")
            if last_dv is None or last_dv < LessonDownloader.VERSION:
                errlog("Warning: there's a new download algorithm,")
                errlog("some lessons may require redownloading.")
                errlog()
            elif last_dv > LessonDownloader.VERSION:
                errlog("Warning: you're using an old download algorithm!")
                if not ARGS.forceolder:
                    errlog("Aborting...")
                    return False
                errlog()
            last_dv = last_dv or LessonDownloader.VERSION
            self.cache["options"]["downloaderVersion"] = max(last_dv, LessonDownloader.VERSION)
        return True

    def load(self):
        if not ARGS.onlyrename:
            self.cred, self.credkey = read_credentials(ARGS.idpath)
            log()
            self.cookies = read_cookies(ARGS.cookiespath, self.credkey)
        ensure_directory(ARGS.syncdir)
        self.cachefile = Path(ARGS.syncdir, "sync.json")
        self.cache = read_cache(self.cachefile)
        self._clean_empty_lessons()
        self.lff = self.cache["options"].get("lessonsFilenameFormat", BETTER_FILENAME_FMT)
        self.num_failed_courses = 0
        self.num_failed_lessons = 0

    def launch(self) -> bool:
        if not self._check_version():
            return False
        log("Synchronizing lessons...")

        def run():
            if ARGS.nosearch:  # do not launch bot, just use manual requests
                self.bot = UniVRBot(self.cred, self.cookies)
                self._main_sync()
            else:
                with UniVRBot(self.cred, self.cookies) as bot:
                    self.bot = bot
                    self._save_cookies()
                    self._main_sync()

        def wrap_net_err():
            return Synchronizer.try_except_network_error(run)

        if ARGS.onlyrename:
            self._main_sync()
        else:
            ok, _1 = wrap_net_err()  # waitOnError(wrap_net_err)
            if not ok:
                return False
            self._save_cookies()

        logger.reset_level()
        log("Done.")
        if self.num_failed_courses > 0:
            log("Warning: unable to elaborate", self.num_failed_courses, "courses.")
        if self.num_failed_lessons > 0:
            log("Warning: unable to elaborate", self.num_failed_lessons, "lessons.")

        return True

    def unload(self):
        self._save_cache()
        for k in "cred credkey cachefile cache cookies bot".split():
            self.__dict__[k] = None

    @staticmethod
    def try_except_network_error(func, *args, **kwargs) -> Tuple[bool, Any]:
        try:
            return True, func(*args, **kwargs)
        except WebDriverException as neterr:
            logger.reset_level()
            errlog()
            errlog("Cannot continue: Web driver exception.")
            with logger:
                if "about:neterror?" in neterr.msg:
                    query = urlparse(re.search(r"about:neterror\?.+$", neterr.msg)[0]).query
                    errlog("Network error:", pointer=True)
                    with logger:
                        errlog(parse_qs(query)["d"][0])
                    return False, None
                if isinstance(neterr, TimeoutException):
                    errlog("Unable to find a page DOM element in time.", pointer=True)
                    errlog()
            raise neterr
