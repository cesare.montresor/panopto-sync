# pylint: disable=bad-continuation
# pylint: disable=bad-whitespace
# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=no-self-use

import math

# import datetime as dt
from enum import Enum, auto
from core import simple_class
from logs import log
from caterpillar.events import (
    SegmentDownloadSucceededEvent,
    SegmentDownloadFailedEvent,
    SegmentsDownloadInitiatedEvent,
    SegmentsDownloadFinishedEvent,
)
from caterpillar.events import Event as CEvent

# lessons contains json cached objects
Course = simple_class("name name_clean id year lessons skip search_raws", "Course")  # in-cache
# tag: {('DV','OBJECT'), ('AUDIO','SCREEN'?)}
Timestamp = simple_class("kind isslide number caption content time thumburl slideurl", "Timestamp")
Segment = simple_class("start end map_to", "Segment")
Stream = simple_class("url name offset tag segments", "Stream")
# Comment = simple_class("user text time creation", "Comment")
Lesson = simple_class(
    """
    courseid course   id
    name name_fixed  is_raw
    date date_name date_better
    authors   duration   hint
    streams timestamps comments
    filename filename2 cached
""",
    "Lesson",
)


class LessonHint(Enum):
    UNKNOWN = auto()
    SIMPLE = auto()
    AUDIO_SLIDES = auto()
    MERGED_SINGLE = auto()
    AUDIO_SCREEN = auto()
    WEBCAM_SCREEN = auto()
    WEBCAM_SCREEN_MERGED = auto()

    def has_standalone_webcam(self) -> bool:
        return self in (LessonHint.WEBCAM_SCREEN, LessonHint.WEBCAM_SCREEN_MERGED)

    def stream0_has_video(self) -> bool:
        return self in (
            LessonHint.SIMPLE,
            LessonHint.MERGED_SINGLE,
            LessonHint.WEBCAM_SCREEN,
            LessonHint.WEBCAM_SCREEN_MERGED,
        )

    @classmethod
    def get_new_hint(cls, lesson: Lesson) -> Enum:
        if lesson.is_raw:
            return LessonHint.SIMPLE

        lens = len(lesson.streams)
        slidef = lambda t: t.isslide
        if lens == 1:
            s0 = lesson.streams[0]
            slides = list(filter(slidef, lesson.timestamps))
            if s0.tag == "AUDIO" and len(slides) > 0:
                return LessonHint.AUDIO_SLIDES
            if s0.tag == "DV":
                return LessonHint.MERGED_SINGLE

        elif lens == 2:
            s0, s1 = lesson.streams[0 : 1 + 1]
            tagtuple = (s0.tag, s1.tag)
            if tagtuple == ("AUDIO", "SCREEN"):
                return LessonHint.AUDIO_SCREEN
            if tagtuple == ("DV", "OBJECT"):
                return LessonHint.WEBCAM_SCREEN

        elif lens == 3:
            s0, s1, s2 = lesson.streams[0 : 2 + 1]
            tagtuple = (s0.tag, s1.tag, s2.tag)
            if tagtuple == ("DV", "OBJECT", "OBJECT"):
                return LessonHint.WEBCAM_SCREEN_MERGED

        return LessonHint.UNKNOWN


class EnterExit:
    def __init__(self, enter, exit_):
        self.enter = enter
        self._exit = exit_

    def __enter__(self):
        self.enter()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._exit()


# noinspection PyMethodMayBeStatic
class ErrorLogger:
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        log(msg)


class ProgressReporter:
    STATUS_DOWNLOADING = "downloading"
    STATUS_FINISHED = "finished"

    def __init__(self, callback):
        self.segment_count = 0
        self.handled_count = 0
        self.downloaded_bytes = 0
        self.callback = callback

    def event_hook(self, event: CEvent) -> None:
        status = ProgressReporter.STATUS_DOWNLOADING  # default

        # An alternative to isinstance: check event.event_type against
        # the EventType enum.
        if isinstance(event, SegmentsDownloadInitiatedEvent):
            self.segment_count = event.segment_count
        elif isinstance(event, SegmentDownloadSucceededEvent):
            self.handled_count += 1
            self.downloaded_bytes += event.path.stat().st_size
        elif isinstance(event, SegmentDownloadFailedEvent):
            self.handled_count += 1
        elif isinstance(event, SegmentsDownloadFinishedEvent):
            status = ProgressReporter.STATUS_FINISHED

        if status is not None:
            estimate = self.downloaded_bytes / self.handled_count * self.segment_count if self.handled_count > 0 else 0
            data = dict(
                status=status,
                handled_count=self.handled_count,
                segment_count=self.segment_count,
                downloaded_bytes=self.downloaded_bytes,
                total_bytes_estimate=math.ceil(estimate),
            )
            self.callback(data)
