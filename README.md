# PanoptoSync

Automatically downloads and synchronizes UniVR Panopto lessons.

## Requirements

Here are reported only tested versions; previous versions *may* work, future versions should work.

| **Kind** | **Description** |
|:--|:--|
| pkg | Install with your package manager / Windows EXE |
| pip | Install with `pip` |

| **Name** | **Kind** | **Version** | **Reason** |
|:--|:--:|:--|:--|
| Python         | [pkg][download-py3]    | 3.8.2       | |
| Firefox        | [pkg][download-ff]     | 74.0.1      | Web browser |
| geckodriver    | [pkg][download-gkdr]   | 0.26.0      | Web driver for scraping (bot core) |
| youtube-dl     | [pkg][download-ytdl]   | 2020.03.24  | Download raw videos |
| FFMpeg         | [pkg][download-ffmpeg] | 4.2.2       | Manipulate streams, obtain info |
| MKVToolNIX     | [pkg][download-mkvtn]  | v45.0.0 (*Heaven in Pennies*) | Merge streams and chapters in a single `mkv` container |
| `pycryptodome`    | pip | 3.9.7     | Store encrypted credentials and cookies on your disk |
| `selenium`        | pip | 3.141.0   | Interact with Firefox Web driver |
| `progress`        | pip | 1.6       | Animated CLI progress bars and spinners |
| `colorama`        | pip | 0.4.4     | Cross-platform support for ANSI escapes |
| `ffmpeg-python`   | pip | 0.2.0     | Put FFMpeg arguments together in an easy way |
| `youtube-dl`      | pip | 2020.3.24 | Attath to youtube-dl from Python |
| `caterpillar-hls` | pip | [custom][download-caterpillar] | Parallel download of lessons streams |

[download-py3]:    https://www.python.org/downloads/
[download-ff]:     https://www.mozilla.org/firefox/download/thanks/
[download-gkdr]:   https://github.com/mozilla/geckodriver/releases
[download-ytdl]:   https://ytdl-org.github.io/youtube-dl/download.html
[download-ffmpeg]: https://ffmpeg.org/download.html
[download-mkvtn]:  https://mkvtoolnix.download/downloads.html
[download-caterpillar]: https://github.com/Microeinstein/caterpillar

### One-liners

#### Pip requirements

```shell
$ pip install -r requirements.txt
```

#### Native dependencies

##### ArchLinux

```shell
$ sudo pacman -S python firefox geckodriver youtube-dl ffmpeg mkvtoolnix-cli
```

##### Ubuntu

```shell
$ sudo apt install python3 firefox firefox-geckodriver youtube-dl ffmpeg mkvtoolnix
```

##### Windows ([chocolatey](https://chocolatey.org/install#install-step2))

```shell
> choco install python3 firefox selenium-gecko-driver youtube-dl ffmpeg mkvtoolnix
```

## Usage

### Command-line syntax

Main script: `panoptoSync.py`

There are global arguments and subcommands; each subcommand has its own arguments.

You can see all of them with
```shell
$ ./panoptoSync.py -h
$ ./panoptoSync.py --help
$ ./panoptoSync.py <ACTION> -h
```

You may have to explicitly state the python interpreter:
```shell
$ python panoptoSync.py
```

### Running

1. Enter your University credentials and a second password to store them (to do just once)

```shell
$ ./panoptoSync.py genid
```

1. Synchronize your lessons (enter the second password first)

```shell
$ ./panoptoSync.py sync
Credentials password: 
Accepted.

Synchronizing lessons...
Web driver has started.
[...]
```

### Raw lessons

If a course contains raw MP4 lessons (not on panopto), make sure it has an entry in `sync.json`,
then add the attribute `"search_raws": true` to the entry. Make sure the syntax is correct.

Example:
```json
    ...
    "number": {
        "lessons": {
            ...
        },
        "name": "Course with raw lessons (2020/2021)",
        "search_raws": true,
        "skip": null,
        "year": 2020
    },
    ...
```

### Skipping courses

When a course ends or you no longer have interest in keeping it in sync,
you can make it skip from all operations. To do so, set the attribute `"skip": true` to its entry.

Example:
```json
    ...
    "number": {
        "lessons": {
            ...
        },
        "name": "Old course (2019/2020)",
        "search_raws": null,
        "skip": true,
        "year": 2019
    },
    ...
```

### Filtering courses

In case you're in a rush and temporarly want to sync just one or two courses,
you can use the `--courses` flag: it accepts multiple case-insensitive regexes, results of which will be joined by OR.

Example:
```shell
$ ./panoptoSync.py sync --courses  'archi.+elab.+\(2020'  'sicur.+reti.*\(2021'
```

## Troubleshooting

* | There's a connection error, but my internet is working. |
  |:--|
  | University servers are offline, in maintenance or rebooting; retry later. |

* | I got `Unable to log in.` but my credentials are correct. |
  |:--|
  | Try deleting your cookies file (`default.cookies`); sometimes it's a server-side problem. |

* | I got a different error. |
  |:--|
  | Please create an Issue or contact us through Telegram. |

## Contacts

[Telegram group](https://t.me/joinchat/GRpMmE0T-7fcxupH)

## Special thanks

* [Lorenzo Bonanni](https://github.com/LorenzoBonanni)

...and everyone else who supported me.
