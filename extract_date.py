import datetime as dt
import itertools
import re
from datetime import datetime
from typing import Optional, Tuple

from classes import Lesson
from title_format import clean_invalid_name
from logs import errlog


CURR_YEAR = str(datetime.now().year)
CURR_CENTURY = CURR_YEAR[:-2]
CURR_MILL = CURR_YEAR[:-3]


MONTHS = (
    ("january", "gennaio"),
    ("february", "febbraio"),
    ("march", "marzo"),
    ("april", "aprile"),
    ("may", "maggio"),
    ("june", "giugno"),
    ("july", "luglio"),
    ("august", "agosto"),
    ("september", "settembre"),
    ("october", "ottobre"),
    ("november", "novembre"),
    ("december", "dicembre"),
)
DAYS = (
    ("monday", "lunedì"),
    ("tuesday", "martedì"),
    ("wednesday", "mercoledì"),
    ("thursday", "giovedì"),
    ("friday", "venerdì"),
    ("saturday", "sabato"),
    ("sunday", "domenica"),
)
RGXP_KNOWN_DAYS = "|".join(t[:3] for t in set(itertools.chain.from_iterable(DAYS)))
RGXP_KNOWN_MONTHS = "|".join(t[:3] for t in set(itertools.chain.from_iterable(MONTHS)))


def day_from_text(day: str) -> Optional[int]:
    day = day.lower()
    le = len(day)
    for i, names in enumerate(DAYS):
        for name in names:
            name = name[:le]
            if name[-1] == "ì":
                day = day[:-1]
                name = name[:-1]
            if day == name:
                return i + 1
    return None


def month_from_text(month: str) -> Optional[int]:
    month = month.lower()
    le = len(month)
    for i, names in enumerate(MONTHS):
        for name in names:
            if month == name[:le]:
                return i + 1
    return None


def re_date(*args):
    return re.compile("".join(args), re.IGNORECASE)


# fmt: off
RGX_NAME_DATE_FORMATS = [
    # 0
    # (longyear)/(longyear)
    re_date(
        # Non valido per 2999/3000, ma tanto a quel tempo Panopto
        # non esisterà già più; meglio avere qualcosa di più restrittivo
        r"(", CURR_MILL, r"[0-9]{3})"
        r"[/-]"
        r"(", CURR_MILL, r"[0-9]{3})"
    ),

    # 1
    # (?:dayname), _(monthname) (day) (longyear) (?:at (hour minute AM/PM))?
    re_date(
        r"(?:", RGXP_KNOWN_DAYS, r")[a-zì]*"
        r", +\W*"
        r"((?:", RGXP_KNOWN_MONTHS, r")[a-z]*)"
        r" +"
        r"([0-9]{1,2})"
        r" +"
        r"(", CURR_MILL, r"[0-9]{3})"
        r"(?: +at ([0-9]{1,2}([\.:;])[0-9]{1,2} [AP]M))?"
    ),

    # 2
    # (?:dayname) _(day)(separator)(month|monthname)\2(year) at (hour)(separator2)(minute)(?:\6second)?
    re_date(
        r"(?:", RGXP_KNOWN_DAYS, r")[a-zì]*"
        r" +\W*"
        r"(0?[1-9]|[12][0-9]|3[01])"
        r"([ \.\\\-/])"  # \2
        r"(0?[1-9]|1[0-2]|(?:", RGXP_KNOWN_MONTHS, r")[a-z]*)"
        r"\2"
        r"((?:", CURR_CENTURY, r")?[0-9]{2})"
        r" +at +"
        r"([0-9]{1,2})"
        r"([\.:;])"      # \6
        r"([0-9]{1,2})"
        r"(?! +[AP]M)"
        r"(?:\6[0-9]{1,2}(?! +[AP]M))?"
    ),

    # 3
    # ISO 8601
    # (longyear)(separator)(month)\2(day)
    re_date(
        r"(", CURR_MILL, r"[0-9]{3})"
        r"([\\\-/])"
        r"(0[1-9]|1[0-2])"
        r"\2"
        r"(0[1-9]|[12][0-9]|3[01])"
    ),

    # 4
    # (?:dayname)?_(day)(separator)+(month|monthname)\2+(year)
    re_date(
        r"(?:(?:", RGXP_KNOWN_DAYS, r")[a-zì]*"
        r"\W*)?"
        r"(0?[1-9]|[12][0-9]|3[01])"
        r"([ \.\\\-/])+"
        r"(0?[1-9]|1[0-2]|(?:", RGXP_KNOWN_MONTHS, r")[a-z]*)"
        r"\2+"
        r"((?:", CURR_CENTURY, r")?[0-9]{2})"
    ),

    # 5
    # (?:dayname)?_(day)_(month|monthname)
    re_date(
        r"(?:(?:", RGXP_KNOWN_DAYS, r")[a-zì]*"
        r"\W*)?"
        r"(0?[1-9]|[12][0-9]|3[01])"
        r"[ \.\\\-/]"
        r"(0?[1-9]|1[0-2]|(?:", RGXP_KNOWN_MONTHS, r")[a-z]*)"
    ),

    # 6
    # needs semantic approach -> "210304"
    # (year)(month)(day)
    re_date(
        r"(", CURR_YEAR[-2:], r")"
        r"(0[1-9]|1[0-2])"
        r"(0[1-9]|[12][0-9]|3[01])"
    ),

    # 7
    # (day)(separator)*(month|monthname)\2*(year)
    re_date(
        r"(0?[1-9]|[12][0-9]|3[01])"
        r"([ \.\\\-/])*"
        r"(0?[1-9]|1[0-2]|(?:", RGXP_KNOWN_MONTHS, r")[a-z]*)"
        r"\2*"
        r"((?:", CURR_CENTURY, r")?[0-9]{2})"
    ),

    # 8
    # (day)(monthname)(year)?
    re_date(
        r"(0?[1-9]|[12][0-9]|3[01])"
        r"((?:", RGXP_KNOWN_MONTHS, r")[a-z]*)"
        r"(?:((?:", CURR_CENTURY, r")?[0-9]{2}))?"
    ),
]
# fmt: on


def find_known_date_format(text: str) -> Optional[Tuple]:
    """
    Returned:
        (pattern_num, match, year?, month?, day?, hour?, minute?)
    """
    nint = lambda n: int(n) if n is not None else None

    def nyear(t):
        if t is None:
            return None
        if len(t) == 2:
            t = CURR_CENTURY + t
        return int(t)

    def nmonth(t):
        if t is None:
            return None
        if t.isalpha():
            return month_from_text(t)
        return int(t)

    for ir, rgx in enumerate(RGX_NAME_DATE_FORMATS):
        m = rgx.search(text)
        if m is None:
            continue

        if ir == 0:
            # make subsequent patterns to ignore this part, by filtering out the result
            text = text[: m.start()] + text[m.end() :]
            continue

        if ir == 1:
            hour, minute = None, None
            if m[4] is not None:
                d = datetime.strptime(m[4], "".join(("%I", m[5], "%M %p")))
                hour, minute = d.hour, d.minute
            return ir, m, nyear(m[3]), nmonth(m[1]), nint(m[2]), hour, minute

        if ir == 2:
            return (
                ir,
                m,
                nyear(m[4]),
                nmonth(m[3]),
                nint(m[1]),
                nint(m[5]),
                nint(m[7]),
            )
        if ir == 3:
            return ir, m, nyear(m[1]), nmonth(m[3]), nint(m[4]), None, None
        if ir == 4:
            return ir, m, nyear(m[4]), nmonth(m[3]), nint(m[1]), None, None
        if ir == 5:
            return ir, m, None, nmonth(m[2]), nint(m[1]), None, None
        if ir == 6:
            return ir, m, nyear(m[1]), nmonth(m[2]), nint(m[3]), None, None
        if ir == 7:
            return ir, m, nyear(m[4]), nmonth(m[3]), nint(m[1]), None, None
        if ir == 8:
            return ir, m, nyear(m[3]), nmonth(m[2]), nint(m[1]), None, None

    return None


def extract_date_from_name(lesson: Lesson) -> Tuple[Optional[dt.datetime], Optional[str]]:
    if lesson.name is None:
        return lesson.date, lesson.name

    def datedefault(v, k, d, n):
        if v is not None:
            return v
        if d is not None:
            return d.__getattribute__(k)
        return n.__getattribute__(k)

    date = None
    name = lesson.name
    t = find_known_date_format(name)
    if t is not None:
        _1, match, year, month, day, hour, minute = t
        errlog(_1, match, year, month, verbos=4)
        now = dt.datetime.now()
        year = datedefault(year, "year", lesson.date, now)
        month = datedefault(month, "month", lesson.date, now)
        day = datedefault(day, "day", lesson.date, now)
        hour = datedefault(hour, "hour", lesson.date, now)
        minute = datedefault(minute, "minute", lesson.date, now)
        date = dt.datetime(
            max(1970, year),
            max(1, month),
            max(1, day),
            max(0, hour),
            max(0, minute),
        )
        # remove date from name
        name1 = name[: match.start()] + " "
        name2 = name[match.end() :]
        name = clean_invalid_name(name1, name2)
    return date, name


def assign_better_date_and_name(lesson: Lesson):
    lesson.date_name, lesson.name_fixed = extract_date_from_name(lesson)
    ld = lesson.date
    ldn = lesson.date_name
    if ldn is not None:
        lesson.date_better = ldn
        # ldb = lesson.date_better
        # unknown time?
        if (ldn.hour, ldn.minute, ldn.second) == (0, 0, 0):
            # same date?
            if (ldn.year, ldn.month, ldn.day) == (ld.year, ld.month, ld.day):
                # copy time
                lesson.date_better = ldn.replace(hour=ld.hour, minute=ld.minute, second=ld.second)
            else:
                # prefer lesson.date
                lesson.date_better = ld
    else:
        lesson.date_better = ld
